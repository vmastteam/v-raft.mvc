<?php


namespace App\Repositories\Def;


use App\Models\Def;

class DefRepository implements IDefRepository
{
    protected $def = null;

    public function __construct(Def $def)
    {
        $this->def = $def;
    }

    public function create($data, $quality)
    {
        $def = new Def($data);
        $def->quality()->associate($quality);
        return $def;
    }

    public function get($id)
    {
        return $this->def::find($id);
    }

    public function getWith($id, $relation)
    {
        return $this->def::with($relation)->find($id);
    }

    public function getByLang($lang)
    {
        return $this->def::with("quality.rubric")
            ->get()
            ->where("quality.rubric.lang", $lang);
    }

    public function delete(&$self)
    {
        return $self->delete();
    }

    public function save(&$self)
    {
        return $self->save();
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->def, $method], $args);
    }
}