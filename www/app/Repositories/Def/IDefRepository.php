<?php


namespace App\Repositories\Def;


interface IDefRepository
{
    public function create($data, $quality);

    public function get($id);

    public function getWith($id, $relation);

    public function getByLang($lang);

    public function delete(&$self);

    public function save(&$self);
}