<?php


namespace App\Repositories\Pdf;


interface IPdfRepository
{
    public function download($rubric, $type, $cjk);
}