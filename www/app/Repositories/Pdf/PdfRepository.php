<?php


namespace App\Repositories\Pdf;


use TCPDF;

class PdfRepository implements IPdfRepository
{
    protected $pdf = null;

    public function __construct()
    {
        $this->pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    }

    public function download($rubric, $type, $cjk)
    {
        $type_txt = $type == "orig" ? "original" : "eng";
        $lang_title = $rubric->language ? $rubric->language->langName.
            ($rubric->language->langName != $rubric->language->angName &&
            $rubric->language->angName != "" ? " (".$rubric->language->angName.")" : "") : $rubric->lang;

        // set document information
        $this->pdf->SetCreator(PDF_CREATOR);
        $this->pdf->SetAuthor('V-RAFT');
        $this->pdf->SetTitle(__("rubric")." - ".$lang_title);
        $this->pdf->SetSubject(__($type_txt));

        // remove default header/footer
        $this->pdf->setPrintHeader(false);
        $this->pdf->setPrintFooter(true);

        $this->pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set default monospaced font
        $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set auto page breaks
        $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // ---------------------------------------------------------

        $this->pdf->setFontSubsetting(true);

        $this->pdf->SetFont('freeserif', 'B', 18);

        // add a page
        $this->pdf->AddPage();

        // set cell margins
        $this->pdf->setCellMargins(1, 1, 1, 1);

        $title = __("rubric")." - ".$lang_title;
        $this->pdf->MultiCell(0, 5, $title, 0, 'C', 0, 1, '', '', true);

        $this->pdf->SetFont('freeserif', '', 14);
        $this->pdf->setColor('text', 140, 140, 140);
        $this->pdf->MultiCell(0, 5, __($type_txt), 0, 'C', 0, 1, '', '', true);

        $this->pdf->setRTL($rubric->language ? $rubric->language->direction == "rtl" : false);

        $italic = $rubric->language ? ($rubric->language->direction == "ltr" || $type == "eng" ? "I" : "") : "";
        $align = $rubric->language ? ($rubric->language->direction == "ltr" || $type == "eng" ? "L" : "R") : "L";
        $font = !$cjk ? "freeserif" : "cid0" . $cjk;

        $tr=1;
        foreach($rubric->qualities as $quality)
        {
            $q = $type == "orig" ? $quality->content : $quality->eng;

            $this->pdf->SetFont($font, 'B', 12);
            $this->pdf->setCellMargins(1, ($tr == 1 ? 10 : 1), 1, 1);

            $this->pdf->setColor('text', 51, 122, 183);
            $this->pdf->MultiCell(0, 5, sprintf('%01d', $tr).". ".$q, 0, $align, 0, 1, '', '', true);

            $df=1;
            foreach($quality->defs as $def)
            {
                $d = $type == "orig" ? $def->content : $def->eng;

                if($rubric->language)
                {
                    if($rubric->language->direction == "ltr" || $type == "eng")
                        $this->pdf->setCellMargins(15, 1, 1, 1);
                    else
                        $this->pdf->setCellMargins(1, 1, 15, 1);
                }
                else
                {
                    $this->pdf->setCellMargins(15, 1, 1, 1);
                }

                $this->pdf->SetFont($font, '', 12);
                $this->pdf->setColor('text', 68, 157, 68);
                $this->pdf->MultiCell(0, 5, sprintf('%01d', $df).". ".$d, 0, $align, 0, 1, '', '', true);

                $ms=1;
                foreach($def->measurements as $measurement)
                {
                    $m = $type == "orig" ? $measurement->content : $measurement->eng;

                    if($rubric->language)
                    {
                        if($rubric->language->direction == "ltr" || $type == "eng")
                            $this->pdf->setCellMargins(30, 1, 1, 1);
                        else
                            $this->pdf->setCellMargins(1, 1, 30, 1);
                    }
                    else
                    {
                        $this->pdf->setCellMargins(30, 1, 1, 1);
                    }

                    $this->pdf->SetFont($font, $italic, 12);
                    $this->pdf->setColor('text', 236, 151, 31);
                    $this->pdf->MultiCell(0, 5, sprintf('%01d', $ms).". ".$m, 0, $align, 0, 1, '', '', true);

                    $ms++;
                }

                $df++;
            }

            $tr++;
        }

        // ---------------------------------------------------------

        //Close and output PDF document
        $this->pdf->Output('rubric_'.$rubric->lang.'_'.$type.'.pdf', 'D');
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->pdf, $method], $args);
    }
}