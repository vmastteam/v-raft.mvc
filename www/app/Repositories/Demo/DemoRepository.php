<?php


namespace App\Repositories\Demo;


class DemoRepository implements IDemoRepository
{
    protected $demo = null;

    public function __construct()
    {

    }

    public function getRubric()
    {
        $rubric = [
            "qualities" => [
                [
                    "content" => "01. 无障碍",
                    "eng" => "01. Accessible",
                    "defs" => [
                        [
                            "content" => "01. 创建必要的格式。",
                            "eng" => "01. Created in necessary formats.",
                            "measurements" => [
                                [
                                    "content" => "01. 是否以必要的格式创建？",
                                    "eng" => "01. Is it created in necessary formats?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 轻松复制和分发。",
                            "eng" => "02. Easily reproduced and distributed.",
                            "measurements" => [
                                [
                                    "content" => "01. 是否容易复制？",
                                    "eng" => "01. Is it easily reproduced?"
                                ],
                                [
                                    "content" => "02. 它容易分发吗？",
                                    "eng" => "02. Is it easily distributed?"
                                ]
                            ]
                        ],
                        [
                            "content" => "03. 适当的字体，大小和布局。",
                            "eng" => "03. Appropriate font, size and layout.",
                            "measurements" => [
                                [
                                    "content" => "01. 它是在适当的字体，大小和布局？",
                                    "eng" => "01. Is it in the appropriate font, size and layout?"
                                ]
                            ]
                        ],
                        [
                            "content" => "04. 编辑。",
                            "eng" => "04. Editable.",
                            "measurements" => [
                                [
                                    "content" => "01. 它是可编辑的吗？",
                                    "eng" => "01. Is it editable?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "02. 可信",
                    "eng" => "02. Faithful",
                    "defs" => [
                        [
                            "content" => "01. 反映原文。",
                            "eng" => "01. Reflects contentinal Text.",
                            "measurements" => [
                                [
                                    "content" => "01. 是否反映原文？",
                                    "eng" => "01. Does in reflect contentinal text?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 对希腊语和希伯来语真实。",
                            "eng" => "02. True to Greek and Hebrew.",
                            "measurements" => [
                                [
                                    "content" => "01. 对希腊文和希伯来文来说，这是真的吗？",
                                    "eng" => "01. Is it true to Greek and Hebrew?"
                                ]
                            ]
                        ],
                        [
                            "content" => "03. 不添加或删除。",
                            "eng" => "03. Does not have additions or deletions.",
                            "measurements" => [
                                [
                                    "content" => "01. 它有添加或删除吗？",
                                    "eng" => "01. Does it have additions or deletions?"
                                ]
                            ]
                        ],
                        [
                            "content" => "04. 保留上帝的名字。",
                            "eng" => "04. Names of God retained.",
                            "measurements" => [
                                [
                                    "content" => "01. 上帝的名字是否保留？",
                                    "eng" => "01. Are the names of God retained?"
                                ]
                            ]
                        ],
                        [
                            "content" => "05. 准确的关键词/关键词。",
                            "eng" => "05. Accurate key terms/key words.",
                            "measurements" => [
                                [
                                    "content" => "01. 关键术语/单词是否准确？",
                                    "eng" => "01. Are key terms/words accurate?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "03. 文化相关",
                    "eng" => "03. Culturally Relevant",
                    "defs" => [
                        [
                            "content" => "01. 成语是可以理解的。",
                            "eng" => "01. Idioms are understandable",
                            "measurements" => [
                                [
                                    "content" => "01. 成语是否可以理解？",
                                    "eng" => "01. Are idioms understandable?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 适合当地文化的词语。",
                            "eng" => "02. Words and expressions appropriate for local culture.",
                            "measurements" => [
                                [
                                    "content" => "01. 文字和表达是否适合当地文化？",
                                    "eng" => "01. Are words and expressions appropriate for local culture?"
                                ]
                            ]
                        ],
                        [
                            "content" => "03. 反映原来的语言艺术性。",
                            "eng" => "03. Reflects contentinal language artistry.",
                            "measurements" => [
                                [
                                    "content" => "01. 它是否反映了原创语言的艺术性？",
                                    "eng" => "01. Does it reflect contentinal language artistry?"
                                ]
                            ]
                        ],
                        [
                            "content" => "04. 捕捉文学类型。",
                            "eng" => "04. Captures literary genres.",
                            "measurements" => [
                                [
                                    "content" => "01. 文学文体是否准确地被捕获？",
                                    "eng" => "01. Are literary genres captured accurately?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "04. 明确",
                    "eng" => "04. Clear",
                    "defs" => [
                        [
                            "content" => "01. 意思很清楚。",
                            "eng" => "01. Meaning is clear.",
                            "measurements" => [
                                [
                                    "content" => "01. 意思是否清楚？",
                                    "eng" => "01. Is the meaning clear?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 使用共同的语言。",
                            "eng" => "02. Uses common language.",
                            "measurements" => [
                                [
                                    "content" => "01. 它使用通用语言吗？",
                                    "eng" => "01. Does it use common language?"
                                ]
                            ]
                        ],
                        [
                            "content" => "03. 容易被广泛的受众理解。",
                            "eng" => "03. Easily understood by wide audience.",
                            "measurements" => [
                                [
                                    "content" => "01. 容易被广泛的观众理解吗？",
                                    "eng" => "01. Is it easily understood by a wide audience?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "05. 正确的语法",
                    "eng" => "05. Proper Grammar",
                    "defs" => [
                        [
                            "content" => "01. 遵循语法规范。",
                            "eng" => "01. Follows grammar norms.",
                            "measurements" => [
                                [
                                    "content" => "01. 它遵循语法规范吗？",
                                    "eng" => "01. Does it follow grammar norms?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 正确的标点符号。",
                            "eng" => "02. Correct punctuation.",
                            "measurements" => [
                                [
                                    "content" => "01. 是否使用了正确的标点符号？",
                                    "eng" => "01. Is correct punctuation used?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "06. 一贯",
                    "eng" => "06. Consistent",
                    "defs" => [
                        [
                            "content" => "01. 翻译反映了语境意义。",
                            "eng" => "01. Translation reflects contextual meaning.",
                            "measurements" => [
                                [
                                    "content" => "01. 翻译是否反映了语境意义？",
                                    "eng" => "01. Does the translation reflect contextual meaning?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 不矛盾本身。",
                            "eng" => "02. Does not contradict itself.",
                            "measurements" => [
                                [
                                    "content" => "01. 文本是否自相矛盾？",
                                    "eng" => "01. Does the text contradict itself?"
                                ]
                            ]
                        ],
                        [
                            "content" => "03. 写作风格一致。",
                            "eng" => "03. Writing style consistent.",
                            "measurements" => [
                                [
                                    "content" => "01. 写作风格是否一致？",
                                    "eng" => "01. Is the writing style consistent?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "07. 历史上准确",
                    "eng" => "07. Historically Accurate",
                    "defs" => [
                        [
                            "content" => "01. 所有的名字，日期，地点，事件都是准确的。",
                            "eng" => "01. All names, dates, places, events are accurately represented.",
                            "measurements" => [
                                [
                                    "content" => "01. 所有的名字都是准确的代表?",
                                    "eng" => "01. Are all names accurately represented?"
                                ],
                                [
                                    "content" => "02. 所有的日期都准确地代表了?",
                                    "eng" => "02. Are all dates accurately represented?"
                                ],
                                [
                                    "content" => "03. 所有地方都准确地代表了吗?",
                                    "eng" => "03. Are all places accurately represented?"
                                ],
                                [
                                    "content" => "04. 所有事件都准确地代表了吗?",
                                    "eng" => "04. Are all events accurately represented?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "08. 自然",
                    "eng" => "08. Natural",
                    "defs" => [
                        [
                            "content" => "01. 翻译使用通用和自然的语言。",
                            "eng" => "01. Translation uses common and natural language.",
                            "measurements" => [
                                [
                                    "content" => "01. 翻译是否使用普通和自然的语言？",
                                    "eng" => "01. Does the translation use common and natural language?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 喜欢读/听。",
                            "eng" => "02. Pleasant to read/listen to.",
                            "measurements" => [
                                [
                                    "content" => "01. 读/听是愉快的？",
                                    "eng" => "01. It is pleasant to read/listen to?"
                                ]
                            ]
                        ],
                        [
                            "content" => "03. 易于阅读。",
                            "eng" => "03. Easy to read.",
                            "measurements" => [
                                [
                                    "content" => "01. 是否容易阅读？",
                                    "eng" => "01. Is it easy to read?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "09. 目的",
                    "eng" => "09. Objective",
                    "defs" => [
                        [
                            "content" => "01. 翻译不解释或评论。",
                            "eng" => "01. Translation does not explain or commentate.",
                            "measurements" => [
                                [
                                    "content" => "01. 翻译是解释还是评论？",
                                    "eng" => "01. Does the translation explain or commentate?"
                                ]
                            ]
                        ],
                        [
                            "content" => "02. 翻译没有政治，社会，宗派的偏见",
                            "eng" => "02. Translation is free of political, social, denominational bias.",
                            "measurements" => [
                                [
                                    "content" => "01. 翻译是否没有政治偏见？",
                                    "eng" => "01. Is translation is free of political bias?"
                                ],
                                [
                                    "content" => "02. 翻译是否没有社会偏见？",
                                    "eng" => "02. Is translation is free of social bias?"
                                ],
                                [
                                    "content" => "03. 翻译是否没有教派偏见？",
                                    "eng" => "03. Is translation is free of denominational bias?"
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    "content" => "10. 被广泛接受",
                    "eng" => "10. Widely Accepted",
                    "defs" => [
                        [
                            "content" => "01. 翻译被当地教会广泛接受。",
                            "eng" => "01. Translation is widely accepted by local church.",
                            "measurements" => [
                                [
                                    "content" => "01. 翻译是否被当地教会广泛接受？",
                                    "eng" => "01. Is translation widely accepted by the local church?"
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "language" => [
                "langName" => "Demo",
                "angName" => "Demo",
                "direction" => "ltr"
            ],
            "lang" => "demo"
        ];

        return json_decode(json_encode($rubric));
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->demo, $method], $args);
    }
}