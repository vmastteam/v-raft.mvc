<?php


namespace App\Repositories\Faq;


interface IFaqRepository
{
    public function all();

    public function get($id);

    public function create($data);

    public function delete(&$self);

    public function save(&$self);
}