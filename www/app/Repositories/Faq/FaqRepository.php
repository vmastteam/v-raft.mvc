<?php


namespace App\Repositories\Faq;


use App\Models\Faq;

class FaqRepository implements IFaqRepository
{
    protected $faq = null;

    public function __construct(Faq $faq)
    {
        $this->faq = $faq;
    }

    public function all()
    {
        return $this->faq::orderBy("created_at", "DESC")->get();
    }

    public function create($data)
    {
        $def = new Faq($data);
        return $def;
    }

    public function get($id)
    {
        return $this->faq::find($id);
    }

    public function delete(&$self)
    {
        return $self->delete();
    }

    public function save(&$self)
    {
        return $self->save();
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->faq, $method], $args);
    }
}