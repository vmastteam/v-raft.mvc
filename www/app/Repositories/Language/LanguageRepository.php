<?php


namespace App\Repositories\Language;


use App\Models\Language;
use File;

class LanguageRepository implements ILanguageRepository
{
    protected $language = null;

    public function __construct(Language $language)
    {
        $this->language = $language;
    }

    public function all()
    {
        return $this->language::get()
            ->sortBy("langName");
    }

    public function get($id)
    {
        return $this->language::find($id);
    }

    public function updateFromTd()
    {
        $langs = $this->getNames();

        $this->deleteAll();

        return $this->insert($langs);
    }

    public function deleteAll()
    {
        return $this->language::where("langID", "LIKE", "%%")
            ->delete();
    }

    public function insert($data)
    {
        return $this->language::insert($data);
    }

    private function getFromTd()
    {
        $langs = [];
        $totalPages = 1;

        for($i=0; $i < $totalPages; $i++)
        {
            $url = "http://td.unfoldingword.org/uw/ajax/languages/?draw=1&order[0][column]=0&".
                "order[0][dir]=asc&start=".($i*500)."&length=500&search[value]=0";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $cat = curl_exec($ch);
            curl_close($ch);
            $arr = json_decode($cat);

            if($arr != null)
            {
                if($i == 0)
                {
                    $totalPages = ceil($arr->recordsTotal/500);
                }

                foreach ($arr->data as $lang) {
                    preg_match('/>(.+)<\//', $lang[0], $matches);
                    $lang[0] = $matches[1];
                    $langs[$matches[1]] = $lang[6];
                }
            }
        }

        return $langs;
    }

    private function getNames()
    {
        $final = [];
        $langs = $this->getFromTd();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://td.unfoldingword.org/exports/langnames.json");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $languages = curl_exec($ch);
        curl_close($ch);

        File::put("../app/Templates/Default/Assets/files/langnames.json", $languages);

        $languages = json_decode($languages);

        foreach ($languages as $language) {
            $tmp = [];
            $tmp["langID"] = $language->lc;
            $tmp["langName"] = $language->ln;
            $tmp["angName"] = $language->ang;
            $tmp["isGW"] = $language->gw;
            $tmp["gwLang"] = $language->gw ?
                $language->ln :
                (isset($langs[$language->lc]) && $langs[$language->lc] ? $langs[$language->lc] : "English");
            $tmp["direction"] = $language->ld;

            $final[] = $tmp;
        }

        return $final;
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->language, $method], $args);
    }
}