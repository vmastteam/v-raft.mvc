<?php


namespace App\Repositories\Language;


interface ILanguageRepository
{
    public function all();

    public function get($id);

    /**
     * Update from Translation Database (http://td.unfoldingword.org)
     * @return mixed
     */
    public function updateFromTd();

    public function deleteAll();

    public function insert($data);
}