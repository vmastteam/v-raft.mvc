<?php


namespace App\Repositories\Rubric;


use App\Models\Rubric;

class RubricRepository implements IRubricRepository
{
    protected $rubric = null;

    public function __construct(Rubric $rubric)
    {
        $this->rubric = $rubric;
    }

    public function all()
    {
        return $this->rubric::with("language")
            ->get()
            ->sortBy("language.langName");
    }

    public function get($id)
    {
        return $this->rubric::find($id);
    }

    public function getByLang($lang)
    {
        return $this->rubric::where('lang', $lang)->first();
    }

    public function getByLangWith($lang, $relations)
    {
        return $this->rubric::with($relations)
            ->where('lang', $lang)->first();
    }

    public function getByEmail($lang, $email)
    {
        return $this->rubric::where([
            "email" => $email,
            "lang" => $lang
        ])->first();
    }

    public function getByResetToken($lang, $token)
    {
        return $this->rubric::where([
            "resetToken" => $token,
            "lang" => $lang
        ])->first();
    }

    public function create($data)
    {
        return $this->rubric::create($data);
    }

    public function save(&$self)
    {
        return $self->save();
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->rubric, $method], $args);
    }
}