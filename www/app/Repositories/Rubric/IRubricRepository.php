<?php


namespace App\Repositories\Rubric;


interface IRubricRepository
{
    public function all();

    public function get($id);

    public function getByLang($lang);

    public function getByLangWith($lang, $relations);

    public function getByEmail($lang, $email);

    public function getByResetToken($lang, $token);

    public function create($data);

    public function save(&$self);
}