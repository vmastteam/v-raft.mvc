<?php


namespace App\Repositories\Measurement;


use App\Models\Measurement;

class MeasurementRepository implements IMeasurementRepository
{
    protected $measurement = null;

    public function __construct(Measurement $measurement)
    {
        $this->measurement = $measurement;
    }

    public function create($data, $def)
    {
        $measurement = new Measurement($data);
        $measurement->def()->associate($def);
        return $measurement;
    }

    public function get($id)
    {
        return $this->measurement::find($id);
    }

    public function getWith($id, $relation)
    {
        return $this->measurement::with($relation)->find($id);
    }

    public function getByLang($lang)
    {
        return $this->measurement::with("def.quality.rubric")
            ->get()
            ->where("def.quality.rubric.lang", $lang);
    }

    public function delete(&$self)
    {
        return $self->delete();
    }

    public function save(&$self)
    {
        return $self->save();
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->measurement, $method], $args);
    }
}