<?php


namespace App\Repositories\Measurement;


interface IMeasurementRepository
{
    public function create($data, $def);

    public function get($id);

    public function getWith($id, $relation);

    public function getByLang($lang);

    public function delete(&$self);

    public function save(&$self);
}