<?php


namespace App\Repositories\Quality;


interface IQualityRepository
{
    public function create($data, $rubric);

    public function get($id);

    public function getWith($id, $relations);

    public function getByLang($lang);

    public function getByLangWith($lang, $relations);

    public function delete(&$self);

    public function save(&$self);
}