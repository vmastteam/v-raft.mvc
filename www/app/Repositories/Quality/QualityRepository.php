<?php


namespace App\Repositories\Quality;


use App\Models\Quality;

class QualityRepository implements IQualityRepository
{
    protected $quality = null;

    public function __construct(Quality $quality)
    {
        $this->quality = $quality;
    }

    public function create($data, $rubric)
    {
        $quality = new Quality($data);
        $quality->rubric()->associate($rubric);
        return $quality;
    }

    public function get($id)
    {
        return $this->quality::find($id);
    }

    public function getWith($id, $relations)
    {
        return $this->quality::with($relations)->find($id);
    }

    public function getByLang($lang)
    {
        return $this->quality::with("rubric")
            ->get()
            ->where("rubric.lang", $lang);
    }

    public function getByLangWith($lang, $relations)
    {
        if(!is_array($relations) || empty($relations))
            $relations = ["rubric"];
        elseif(!in_array("rubric", $relations))
            $relations[] = "rubric";

        return $this->quality::with($relations)
            ->get()
            ->where("rubric.lang", $lang);
    }

    public function delete(&$self)
    {
        return $self->delete();
    }

    public function save(&$self)
    {
        return $self->save();
    }

    public function __call($method, $args)
    {
        return call_user_func_array([$this->quality, $method], $args);
    }
}