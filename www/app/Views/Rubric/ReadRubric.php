<?php if (isset($data["rubric"])): ?>
<div>
    <div class="read_rubric_header">
        <span><?php echo ($data["rubric"]->language ? $data["rubric"]->language->langName .
        ($data["rubric"]->language->langName != $data["rubric"]->language->angName &&
        $data["rubric"]->language->angName != "" ? " (".$data["rubric"]->language->angName.")" : "") : $data["rubric"]->lang) ?>
        </span>
        <?php if($data["rubric"]->lang == Session::get("lang")): ?>
        <a class="btn btn-link" href="/">(<?php echo __("edit") ?>)</a>
        <?php endif; ?>
    </div>
    <ul class="nav nav-tabs nav-justified read_rubric_tabs">
        <li role="presentation" id="tab_orig" class="active"><a href="#"><?php echo __("original") ?></a></li>
        <li role="presentation" id='tab_eng'><a href="#"><?php echo __("eng") ?></a></li>
    </ul>
    <div class="read_rubric_qualities">
        <div class="orig download_pdf">
            <span class="glyphicon glyphicon-download-alt"></span>
            <a href="/rubric/<?php echo $data["rubric"]->lang ?>/download/orig/">
                <?php echo __("download_pdf") ?>
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label for="cjk_enable" title="<?php echo __("cjk_font") ?>"><input type="checkbox" id="cjk_enable" /> CJK</label>
            <div class="cjk_options">
                <label><input type="radio" name="cjk" class="cjk" id="cs" checked /> <?php echo __("cjk_cs") ?></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="cjk" class="cjk" id="ct" /> <?php echo __("cjk_ct") ?></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="cjk" class="cjk" id="jp" /> <?php echo __("cjk_jp") ?></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="cjk" class="cjk" id="kr" /> <?php echo __("cjk_kr") ?></label>
            </div>
        </div>
        <div class="eng download_pdf">
            <span class="glyphicon glyphicon-download-alt"></span>
            <a href="/rubric/<?php echo $data["rubric"]->lang ?>/download/eng">
                <?php echo __("download_pdf") ?>
            </a>
        </div>

        <br>

        <?php $tr=1; foreach($data["rubric"]->qualities as $quality): ?>
        <div class="read_rubric_quality orig"
             dir="<?php echo $data['rubric']->language ? $data['rubric']->language->direction : "" ?>">
            <?php echo sprintf('%01d', $tr); ?>. <?php echo $quality->content; ?>
        </div>
        <div class="read_rubric_quality eng"><?php echo sprintf('%01d', $tr); ?>. <?php echo $quality->eng; ?></div>

        <div class="read_rubric_defs">
            <?php $df=1; foreach($quality->defs as $def): ?>
            <div class="read_rubric_def orig"
                 dir="<?php echo $data['rubric']->language ? $data['rubric']->language->direction : "" ?>">
                <?php echo sprintf('%01d', $df); ?>. <?php echo $def->content; ?>
            </div>
            <div class="read_rubric_def eng"><?php echo sprintf('%01d', $df); ?>. <?php echo $def->eng; ?></div>
            
            <div class="read_rubric_measurements">
                <?php $ms=1; foreach($def->measurements as $measurement): ?>
                <div class="read_rubric_measurement orig"
                     dir="<?php echo $data['rubric']->language ? $data['rubric']->language->direction : "" ?>">
                    <?php echo sprintf('%01d', $ms); ?>. <?php echo $measurement->content; ?>
                </div>
                <div class="read_rubric_measurement eng"><?php echo sprintf('%01d', $ms); ?>. <?php echo $measurement->eng; ?></div>
                <?php $ms++; endforeach; ?>
            </div>
            <?php $df++; endforeach; ?>
        </div>
        <?php $tr++; endforeach; ?>
    </div>
</div>
<?php else: ?>
<div class="no_rubric_error">
    <?php echo __("rubric_not_exist_error"); ?>
</div>
<?php endif; ?>
