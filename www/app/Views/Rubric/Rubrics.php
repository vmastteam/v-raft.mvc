<div>
    <div class="lang_filter_box">
        <label><?php echo __("filter") ?>:
            <input type="text" class="form-control" id="lang_filter" placeholder="<?php echo __("filter_lang_tip") ?>" />
        </label>
    </div>

    <?php foreach($data['rubrics'] as $rubric): ?>
        <div>
            <a class="lang_item" href="<?php echo $rubric->lang ?>"><?php echo ($rubric->language ? "[" . $rubric->language->langID . "] " . $rubric->language->langName .
                    ($rubric->language->langName != $rubric->language->angName &&
                        $rubric->language->angName != "" ? " (".$rubric->language->angName.")" : "") : $rubric->lang) ?></a>
        </div>
    <?php endforeach; ?>
</div>