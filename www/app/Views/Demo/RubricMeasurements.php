<div class="rubric_page">
    <h2>
        <?php echo mb_strtoupper(__("measurements")) ?>
        <span>(<?php echo __("demo") ?>)</span>
    </h2>
    <div class="rubric_item_container">
        <div class="rubric_item_header">
            <div class="rubric_orig"><?php echo __("original") ?></div>
            <div class="rubric_eng"><?php echo __("eng") ?></div>
            <div class="clear"></div>
        </div>
        <div class="rubric_qualities">
            <h3 class="level1">01. 无障碍</h3>
            <h3 class="level1 right">Accessible</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 创建必要的格式。</h3>
                <h3 class="level2 right">Created in necessary formats.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="50">
                        <div class="rubric_item_block" data-id="63"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                               rows="2">是否以必要的格式创建？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it created in necessary formats?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 轻松复制和分发。</h3>
                <h3 class="level2 right">Easily reproduced and distributed.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="67">
                        <div class="rubric_item_block" data-id="104"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">是否容易复制？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it easily reproduced?</textarea></div>
                        <div class="rubric_item_block" data-id="105"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它容易分发吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it easily distributed?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">03. 适当的字体，大小和布局。</h3>
                <h3 class="level2 right">Appropriate font, size and layout.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="68">
                        <div class="rubric_item_block" data-id="106"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它是在适当的字体，大小和布局？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it in the appropriate font, size and layout?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">04. 编辑。</h3>
                <h3 class="level2 right">Editable.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="69">
                        <div class="rubric_item_block" data-id="107"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它是可编辑的吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it editable?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">02. 可信</h3>
            <h3 class="level1 right">Faithful</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 反映原文。</h3>
                <h3 class="level2 right">Reflects Original Text.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="71">
                        <div class="rubric_item_block" data-id="108"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">是否反映原文？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does in reflect original text?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 对希腊语和希伯来语真实。</h3>
                <h3 class="level2 right">True to Greek and Hebrew.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="72">
                        <div class="rubric_item_block" data-id="109"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">对希腊文和希伯来文来说，这是真的吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it true to Greek and Hebrew?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">03. 不添加或删除。</h3>
                <h3 class="level2 right">Does not have additions or deletions.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="73">
                        <div class="rubric_item_block" data-id="110"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它有添加或删除吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does it have additions or deletions?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">04. 保留上帝的名字。</h3>
                <h3 class="level2 right">Names of God retained.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="74">
                        <div class="rubric_item_block" data-id="111"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">上帝的名字是否保留？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are the names of God retained?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">05. 准确的关键词/关键词。</h3>
                <h3 class="level2 right">Accurate key terms/key words.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="75">
                        <div class="rubric_item_block" data-id="112"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">关键术语/单词是否准确？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are key terms/words accurate?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">03. 文化相关</h3>
            <h3 class="level1 right">Culturally Relevant</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 成语是可以理解的。</h3>
                <h3 class="level2 right">Idioms are understandable</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="76">
                        <div class="rubric_item_block" data-id="113"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">成语是否可以理解？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are idioms understandable?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 适合当地文化的词语。</h3>
                <h3 class="level2 right">Words and expressions appropriate for local culture.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="77">
                        <div class="rubric_item_block" data-id="114"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">文字和表达是否适合当地文化？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are words and expressions appropriate for local culture?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">03. 反映原来的语言艺术性。</h3>
                <h3 class="level2 right"> Reflects original language artistry.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="78">
                        <div class="rubric_item_block" data-id="115"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它是否反映了原创语言的艺术性？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does it reflect original language artistry?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">04. 捕捉文学类型。</h3>
                <h3 class="level2 right">Captures literary genres.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="79">
                        <div class="rubric_item_block" data-id="116"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">文学文体是否准确地被捕获？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are literary genres captured accurately?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">04. 明确</h3>
            <h3 class="level1 right">Clear</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 意思很清楚。</h3>
                <h3 class="level2 right">Meaning is clear.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="80">
                        <div class="rubric_item_block" data-id="117"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">意思是否清楚？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is the meaning clear?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 使用共同的语言。</h3>
                <h3 class="level2 right">Uses common language.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="81">
                        <div class="rubric_item_block" data-id="118"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它使用通用语言吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does it use common language?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">03. 容易被广泛的受众理解。</h3>
                <h3 class="level2 right">Easily understood by wide audience.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="82">
                        <div class="rubric_item_block" data-id="119"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">容易被广泛的观众理解吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it easily understood by a wide audience?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">05. 正确的语法</h3>
            <h3 class="level1 right">Proper Grammar</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 遵循语法规范。</h3>
                <h3 class="level2 right">Follows grammar norms.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="83">
                        <div class="rubric_item_block" data-id="120"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">它遵循语法规范吗？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does it follow grammar norms?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 正确的标点符号。</h3>
                <h3 class="level2 right">Correct punctuation.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="84">
                        <div class="rubric_item_block" data-id="121"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">是否使用了正确的标点符号？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is correct punctuation used?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">06. 一贯</h3>
            <h3 class="level1 right">Consistent</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 翻译反映了语境意义。</h3>
                <h3 class="level2 right">Translation reflects contextual meaning.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="85">
                        <div class="rubric_item_block" data-id="122"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是否反映了语境意义？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does the translation reflect contextual meaning?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 不矛盾本身。</h3>
                <h3 class="level2 right">Does not contradict itself.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="86">
                        <div class="rubric_item_block" data-id="123"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">文本是否自相矛盾？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does the text contradict itself?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">03. 写作风格一致。</h3>
                <h3 class="level2 right">Writing style consistent.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="87">
                        <div class="rubric_item_block" data-id="124"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">写作风格是否一致？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is the writing style consistent?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">07. 历史上准确</h3>
            <h3 class="level1 right">Historically Accurate</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 所有的名字，日期，地点，事件都是准确的。</h3>
                <h3 class="level2 right">All names, dates, places, events are accurately represented.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="88">
                        <div class="rubric_item_block" data-id="125"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">所有的名字都是准确的代表?</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are all names accurately represented?</textarea></div>
                        <div class="rubric_item_block" data-id="126"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">所有的日期都准确地代表了?</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are all dates accurately represented?</textarea></div>
                        <div class="rubric_item_block" data-id="127"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">所有地方都准确地代表了吗?</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are all places accurately represented?</textarea></div>
                        <div class="rubric_item_block" data-id="128"><span>04.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">所有事件都准确地代表了吗?</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Are all events accurately represented?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">08. 自然</h3>
            <h3 class="level1 right">Natural</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 翻译使用通用和自然的语言。</h3>
                <h3 class="level2 right">Translation uses common and natural language.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="89">
                        <div class="rubric_item_block" data-id="129"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是否使用普通和自然的语言？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does the translation use common and natural language?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 喜欢读/听。</h3>
                <h3 class="level2 right">Pleasant to read/listen to.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="90">
                        <div class="rubric_item_block" data-id="130"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">读/听是愉快的？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">It is pleasant to read/listen to?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">03. 易于阅读。</h3>
                <h3 class="level2 right">Easy to read.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="91">
                        <div class="rubric_item_block" data-id="131"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">是否容易阅读？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is it easy to read?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">09. 目的</h3>
            <h3 class="level1 right">Objective</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 翻译不解释或评论。</h3>
                <h3 class="level2 right">Translation does not explain or commentate.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="92">
                        <div class="rubric_item_block" data-id="132"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是解释还是评论？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Does the translation explain or commentate?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <h3 class="level2">02. 翻译没有政治，社会，宗派的偏见。</h3>
                <h3 class="level2 right">Translation is free of political, social, denominational bias.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="93">
                        <div class="rubric_item_block" data-id="133"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是否没有政治偏见？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is translation is free of political bias?</textarea></div>
                        <div class="rubric_item_block" data-id="134"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是否没有社会偏见？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is translation is free of social bias?</textarea></div>
                        <div class="rubric_item_block" data-id="135"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是否没有教派偏见？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2">Is translation is free of denominational bias?</textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
            <h3 class="level1">10. 被广泛接受</h3>
            <h3 class="level1 right">Widely Accepted</h3>
            <div class="level1 rubric_defs">
                <h3 class="level2">01. 翻译被当地教会广泛接受。</h3>
                <h3 class="level2 right">Translation is widely accepted by local church.</h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="94">
                        <div class="rubric_item_block" data-id="136"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                                rows="2">翻译是否被当地教会广泛接受？</textarea>
                            <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                    title="<?php echo __('delete') ?>"></button>
                            <textarea class="rubric_item_eng" rows="2"></textarea></div>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus"
                                title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <div class="rubric_item_continue">
        <a href="/demo/define" class="btn btn-primary rubric_navigate">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <?php echo __("to_definitions") ?>
        </a>
        <a href="/demo/rubric" class="btn btn-primary rubric_navigate">
            <?php echo __("finish") ?>
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>

<script>
    var isDemo = true;
</script>