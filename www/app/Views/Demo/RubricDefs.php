<div class="rubric_page">
    <h2>
        <?php echo mb_strtoupper(__("definitions")) ?>
        <span>(<?php echo __("demo") ?>)</span>
    </h2>
    <div class="rubric_item_container">
        <div class="rubric_item_header">
            <div class="rubric_orig"><?php echo __("original") ?></div>
            <div class="rubric_eng"><?php echo __("eng") ?></div>
            <div class="clear"></div>
        </div>
        <div class="rubric_qualities">
            <h3 class="level1">01. 无障碍</h3>
            <h3 class="level1 right">Accessible</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="54">
                    <div class="rubric_item_block" data-id="50"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">创建必要的格式。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Created in necessary formats.</textarea></div>
                    <div class="rubric_item_block" data-id="67"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">轻松复制和分发。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Easily reproduced and distributed.</textarea></div>
                    <div class="rubric_item_block" data-id="68"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">适当的字体，大小和布局。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Appropriate font, size and layout.</textarea></div>
                    <div class="rubric_item_block" data-id="69"><span>04.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">编辑。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Editable.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">02. 可信</h3>
            <h3 class="level1 right">Faithful</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="69">
                    <div class="rubric_item_block" data-id="71"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">反映原文。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Reflects Original Text.</textarea></div>
                    <div class="rubric_item_block" data-id="72"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">对希腊语和希伯来语真实。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">True to Greek and Hebrew.</textarea></div>
                    <div class="rubric_item_block" data-id="73"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">不添加或删除。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Does not have additions or deletions.</textarea></div>
                    <div class="rubric_item_block" data-id="74"><span>04.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">保留上帝的名字。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Names of God retained.</textarea></div>
                    <div class="rubric_item_block" data-id="75"><span>05.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">准确的关键词/关键词。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Accurate key terms/key words.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">03. 文化相关</h3>
            <h3 class="level1 right">Culturally Relevant</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="70">
                    <div class="rubric_item_block" data-id="76"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">成语是可以理解的。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Idioms are understandable</textarea></div>
                    <div class="rubric_item_block" data-id="77"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">适合当地文化的词语。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Words and expressions appropriate for local culture.</textarea></div>
                    <div class="rubric_item_block" data-id="78"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">反映原来的语言艺术性。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Reflects original language artistry.</textarea></div>
                    <div class="rubric_item_block" data-id="79"><span>04.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">捕捉文学类型。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Captures literary genres.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">04. 明确</h3>
            <h3 class="level1 right">Clear</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="71">
                    <div class="rubric_item_block" data-id="80"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">意思很清楚。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Meaning is clear.</textarea></div>
                    <div class="rubric_item_block" data-id="81"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">使用共同的语言。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Uses common language.</textarea></div>
                    <div class="rubric_item_block" data-id="82"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">容易被广泛的受众理解。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Easily understood by wide audience.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">05. 正确的语法</h3>
            <h3 class="level1 right">Proper Grammar</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="72">
                    <div class="rubric_item_block" data-id="83"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">遵循语法规范。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Follows grammar norms.</textarea></div>
                    <div class="rubric_item_block" data-id="84"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">正确的标点符号。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Correct punctuation.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">06. 一贯</h3>
            <h3 class="level1 right">Consistent</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="73">
                    <div class="rubric_item_block" data-id="85"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">翻译反映了语境意义。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Translation reflects contextual meaning.</textarea></div>
                    <div class="rubric_item_block" data-id="86"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">不矛盾本身。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Does not contradict itself.</textarea></div>
                    <div class="rubric_item_block" data-id="87"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">写作风格一致。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Writing style consistent.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">07. 历史上准确</h3>
            <h3 class="level1 right">Historically Accurate</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="74">
                    <div class="rubric_item_block" data-id="88"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">所有的名字，日期，地点，事件都是准确的。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">All names, dates, places, events are accurately represented.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">08. 自然</h3>
            <h3 class="level1 right">Natural</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="75">
                    <div class="rubric_item_block" data-id="89"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">翻译使用通用和自然的语言。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Translation uses common and natural language.</textarea></div>
                    <div class="rubric_item_block" data-id="90"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">喜欢读/听。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Pleasant to read/listen to.</textarea></div>
                    <div class="rubric_item_block" data-id="91"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2">易于阅读。</textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2">Easy to read.</textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">09. 目的</h3>
            <h3 class="level1 right">Objective</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="76">
                    <div class="rubric_item_block" data-id="92"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2"></textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2"></textarea></div>
                    <div class="rubric_item_block" data-id="93"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2"></textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2"></textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
            <h3 class="level1">10. 被广泛接受</h3>
            <h3 class="level1 right">Widely Accepted</h3>
            <div class="parent_block">
                <div class="rubric_items" data-type="def" data-parentid="77">
                    <div class="rubric_item_block" data-id="94"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                           rows="2"></textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                                title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2"></textarea></div>
                </div>
                <div class="rubric_item_add">
                    <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
                </div>
            </div>
            <hr>
        </div>
    </div>
    <div class="rubric_item_continue">
        <a href="/demo/qualities" class="btn btn-primary rubric_navigate">
            <span class="glyphicon glyphicon-chevron-left"></span> <?php echo __("to_qualities") ?>
        </a>
        <a href="/demo/measurements" class="btn btn-primary rubric_navigate"> <?php echo __("to_measurements") ?>
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>

<script>
    var isDemo = true;
</script>