<div>
    <div class="read_rubric_header">
        <span>English </span>
        <span style="color: #ff0000;">(<?php echo __("demo") ?>)</span>
    </div>
    <ul class="nav nav-tabs nav-justified read_rubric_tabs">
        <li role="presentation" id="tab_orig" class="active"><a href="#"><?php echo __("original") ?></a></li>
        <li role="presentation" id="tab_eng"><a href="#"><?php echo __("eng") ?></a></li>
    </ul>
    <div class="read_rubric_qualities">
        <div class="orig download_pdf">
            <span class="glyphicon glyphicon-download-alt"></span>
            <a href="/demo/download/orig/"><?php echo __("download_pdf") ?></a>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <label for="cjk_enable" title="<?php echo __("cjk_font") ?>"><input type="checkbox" id="cjk_enable" /> CJK</label>
            <div class="cjk_options">
                <label><input type="radio" name="cjk" class="cjk" id="cs" checked /> <?php echo __("cjk_cs") ?></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="cjk" class="cjk" id="ct" /> <?php echo __("cjk_ct") ?></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="cjk" class="cjk" id="jp" /> <?php echo __("cjk_jp") ?></label>
                &nbsp;&nbsp;
                <label><input type="radio" name="cjk" class="cjk" id="kr" /> <?php echo __("cjk_kr") ?></label>
            </div>
        </div>
        <div class="eng download_pdf">
            <span class="glyphicon glyphicon-download-alt"></span>
            <a href="/demo/download/eng"><?php echo __("download_pdf") ?></a>
        </div>

        <br>

        <div class="read_rubric_quality orig">01. 无障碍</div>
        <div class="read_rubric_quality eng">01. Accessible</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 创建必要的格式。</div>
            <div class="read_rubric_def eng">01. Created in necessary formats.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 是否以必要的格式创建？</div>
                <div class="read_rubric_measurement eng">01. Is it created in necessary formats?</div>
            </div>
            <div class="read_rubric_def orig">02. 轻松复制和分发。</div>
            <div class="read_rubric_def eng">02. Easily reproduced and distributed.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 是否容易复制？</div>
                <div class="read_rubric_measurement eng">01. Is it easily reproduced?</div>
                <div class="read_rubric_measurement orig">02. 它容易分发吗？</div>
                <div class="read_rubric_measurement eng">02. Is it easily distributed?</div>
            </div>
            <div class="read_rubric_def orig">03. 适当的字体，大小和布局。</div>
            <div class="read_rubric_def eng">03. Appropriate font, size and layout.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 它是在适当的字体，大小和布局？</div>
                <div class="read_rubric_measurement eng">01. Is it in the appropriate font, size and layout?</div>
            </div>
            <div class="read_rubric_def orig">04. 编辑。</div>
            <div class="read_rubric_def eng">04. Editable.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 它是可编辑的吗？</div>
                <div class="read_rubric_measurement eng">01. Is it editable?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">02. 可信</div>
        <div class="read_rubric_quality eng">02. Faithful</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 反映原文。</div>
            <div class="read_rubric_def eng">01. Reflects Original Text.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 是否反映原文？</div>
                <div class="read_rubric_measurement eng">01. Does in reflect original text?</div>
            </div>
            <div class="read_rubric_def orig">02. 对希腊语和希伯来语真实。</div>
            <div class="read_rubric_def eng">02. True to Greek and Hebrew.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 对希腊文和希伯来文来说，这是真的吗？</div>
                <div class="read_rubric_measurement eng">01. Is it true to Greek and Hebrew?</div>
            </div>
            <div class="read_rubric_def orig">03. 不添加或删除。</div>
            <div class="read_rubric_def eng">03. Does not have additions or deletions.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 它有添加或删除吗？</div>
                <div class="read_rubric_measurement eng">01. Does it have additions or deletions?</div>
            </div>
            <div class="read_rubric_def orig">04. 保留上帝的名字。</div>
            <div class="read_rubric_def eng">04. Names of God retained.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 上帝的名字是否保留？</div>
                <div class="read_rubric_measurement eng">01. Are the names of God retained?</div>
            </div>
            <div class="read_rubric_def orig">05. 准确的关键词/关键词。</div>
            <div class="read_rubric_def eng">05. Accurate key terms/key words.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 关键术语/单词是否准确？</div>
                <div class="read_rubric_measurement eng">01. Are key terms/words accurate?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">03. 文化相关</div>
        <div class="read_rubric_quality eng">03. Culturally Relevant</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 成语是可以理解的。</div>
            <div class="read_rubric_def eng">01. Idioms are understandable</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 成语是否可以理解？</div>
                <div class="read_rubric_measurement eng">01. Are idioms understandable?</div>
            </div>
            <div class="read_rubric_def orig">02. 适合当地文化的词语。</div>
            <div class="read_rubric_def eng">02. Words and expressions appropriate for local culture.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 文字和表达是否适合当地文化？</div>
                <div class="read_rubric_measurement eng">01. Are words and expressions appropriate for local culture?</div>
            </div>
            <div class="read_rubric_def orig">03. 反映原来的语言艺术性。</div>
            <div class="read_rubric_def eng">03. Reflects original language artistry.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 它是否反映了原创语言的艺术性？</div>
                <div class="read_rubric_measurement eng">01. Does it reflect original language artistry?</div>
            </div>
            <div class="read_rubric_def orig">04. 捕捉文学类型。</div>
            <div class="read_rubric_def eng">04. Captures literary genres.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 文学文体是否准确地被捕获？</div>
                <div class="read_rubric_measurement eng">01. Are literary genres captured accurately?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">04. 明确</div>
        <div class="read_rubric_quality eng">04. Clear</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 意思很清楚。</div>
            <div class="read_rubric_def eng">01. Meaning is clear.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 意思是否清楚？</div>
                <div class="read_rubric_measurement eng">01. Is the meaning clear?</div>
            </div>
            <div class="read_rubric_def orig">02. 使用共同的语言。</div>
            <div class="read_rubric_def eng">02. Uses common language.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 它使用通用语言吗？</div>
                <div class="read_rubric_measurement eng">01. Does it use common language?</div>
            </div>
            <div class="read_rubric_def orig">03. 容易被广泛的受众理解。</div>
            <div class="read_rubric_def eng">03. Easily understood by wide audience.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 容易被广泛的观众理解吗？</div>
                <div class="read_rubric_measurement eng">01. Is it easily understood by a wide audience?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">05. 正确的语法</div>
        <div class="read_rubric_quality eng">05. Proper Grammar</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 遵循语法规范。</div>
            <div class="read_rubric_def eng">01. Follows grammar norms.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 它遵循语法规范吗？</div>
                <div class="read_rubric_measurement eng">01. Does it follow grammar norms?</div>
            </div>
            <div class="read_rubric_def orig">02. 正确的标点符号。</div>
            <div class="read_rubric_def eng">02. Correct punctuation.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 是否使用了正确的标点符号？</div>
                <div class="read_rubric_measurement eng">01. Is correct punctuation used?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">06. 一贯</div>
        <div class="read_rubric_quality eng">06. Consistent</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 翻译反映了语境意义。</div>
            <div class="read_rubric_def eng">01. Translation reflects contextual meaning.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 翻译是否反映了语境意义？</div>
                <div class="read_rubric_measurement eng">01. Does the translation reflect contextual meaning?</div>
            </div>
            <div class="read_rubric_def orig">02. 不矛盾本身。</div>
            <div class="read_rubric_def eng">02. Does not contradict itself.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 文本是否自相矛盾？</div>
                <div class="read_rubric_measurement eng">01. Does the text contradict itself?</div>
            </div>
            <div class="read_rubric_def orig">03. 写作风格一致。</div>
            <div class="read_rubric_def eng">03. Writing style consistent.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 写作风格是否一致？</div>
                <div class="read_rubric_measurement eng">01. Is the writing style consistent?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">07. 历史上准确</div>
        <div class="read_rubric_quality eng">07. Historically Accurate</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 所有的名字，日期，地点，事件都是准确的。</div>
            <div class="read_rubric_def eng">01. All names, dates, places, events are accurately represented.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 所有的名字都是准确的代表?</div>
                <div class="read_rubric_measurement eng">01. Are all names accurately represented?</div>
                <div class="read_rubric_measurement orig">02. 所有的日期都准确地代表了?</div>
                <div class="read_rubric_measurement eng">02. Are all dates accurately represented?</div>
                <div class="read_rubric_measurement orig">03. 所有地方都准确地代表了吗?</div>
                <div class="read_rubric_measurement eng">03. Are all places accurately represented?</div>
                <div class="read_rubric_measurement orig">04. 所有事件都准确地代表了吗?</div>
                <div class="read_rubric_measurement eng">04. Are all events accurately represented?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">08. 自然</div>
        <div class="read_rubric_quality eng">08. Natural</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 翻译使用通用和自然的语言。</div>
            <div class="read_rubric_def eng">01. Translation uses common and natural language.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 翻译是否使用普通和自然的语言？</div>
                <div class="read_rubric_measurement eng">01. Does the translation use common and natural language?</div>
            </div>
            <div class="read_rubric_def orig">02. 喜欢读/听。</div>
            <div class="read_rubric_def eng">02. Pleasant to read/listen to.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 读/听是愉快的？</div>
                <div class="read_rubric_measurement eng">01. It is pleasant to read/listen to?</div>
            </div>
            <div class="read_rubric_def orig">03. 易于阅读。</div>
            <div class="read_rubric_def eng">03. Easy to read.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 是否容易阅读？</div>
                <div class="read_rubric_measurement eng">01. Is it easy to read?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">09. 目的</div>
        <div class="read_rubric_quality eng">09. Objective</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 翻译不解释或评论。</div>
            <div class="read_rubric_def eng">01. Translation does not explain or commentate.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 翻译是解释还是评论？</div>
                <div class="read_rubric_measurement eng">01. Does the translation explain or commentate?</div>
            </div>
            <div class="read_rubric_def orig">02. 翻译没有政治，社会，宗派的偏见</div>
            <div class="read_rubric_def eng">02. Translation is free of political, social, denominational bias.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 翻译是否没有政治偏见？</div>
                <div class="read_rubric_measurement eng">01. Is translation is free of political bias?</div>
                <div class="read_rubric_measurement orig">02. 翻译是否没有社会偏见？</div>
                <div class="read_rubric_measurement eng">02. Is translation is free of social bias?</div>
                <div class="read_rubric_measurement orig">03. 翻译是否没有教派偏见？</div>
                <div class="read_rubric_measurement eng">03. Is translation is free of denominational bias?</div>
            </div>
        </div>
        <div class="read_rubric_quality orig">10. 被广泛接受</div>
        <div class="read_rubric_quality eng">10. Widely Accepted</div>
        <div class="read_rubric_defs">
            <div class="read_rubric_def orig">01. 翻译被当地教会广泛接受。</div>
            <div class="read_rubric_def eng">01. Translation is widely accepted by local church.</div>
            <div class="read_rubric_measurements">
                <div class="read_rubric_measurement orig">01. 翻译是否被当地教会广泛接受？</div>
                <div class="read_rubric_measurement eng">01. Is translation widely accepted by the local church?</div>
            </div>
        </div>
    </div>
</div>