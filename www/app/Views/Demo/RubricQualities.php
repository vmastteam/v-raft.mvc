<div class="rubric_page">
    <h2>
        <?php echo mb_strtoupper(__("qualities")) ?>
        <span>(<?php echo __("demo") ?>)</span>
    </h2>
    <div class="rubric_item_container">
        <div class="rubric_item_header">
            <div class="rubric_orig"><?php echo __("original") ?></div>
            <div class="rubric_eng"><?php echo __("eng") ?></div>
            <div class="clear"></div>
        </div>
        <div class="parent_block">
            <div class="rubric_items" data-type="quality" data-parentid="10">
                <div class="rubric_item_block" data-id="54"><span>01.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">无障碍</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Accessible</textarea></div>
                <div class="rubric_item_block" data-id="69"><span>02.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">可信</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Faithful</textarea></div>
                <div class="rubric_item_block" data-id="70"><span>03.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">文化相关</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Culturally Relevant</textarea></div>
                <div class="rubric_item_block" data-id="71"><span>04.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">明确</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Clear</textarea></div>
                <div class="rubric_item_block" data-id="72"><span>05.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">正确的语法</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Proper Grammar</textarea></div>
                <div class="rubric_item_block" data-id="73"><span>06.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">一贯</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Consistent</textarea></div>
                <div class="rubric_item_block" data-id="74"><span>07.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">历史上准确</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2">Historically Accurate</textarea></div>
                <div class="rubric_item_block" data-id="75"><span>08.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">自然</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2"></textarea></div>
                <div class="rubric_item_block" data-id="76"><span>09.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2">目的</textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2"></textarea></div>
                <div class="rubric_item_block" data-id="77"><span>10.</span> <textarea class="rubric_item_orig"
                                                                                       rows="2"></textarea>
                    <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove"
                            title="<?php echo __('delete') ?>"></button>
                    <textarea class="rubric_item_eng" rows="2"></textarea></div>
            </div>
            <div class="rubric_item_add">
                <button class="btn btn-success glyphicon glyphicon-plus" title="<?php echo __('add') ?>"></button>
            </div>
        </div>
        <hr>
    </div>
    <div class="rubric_item_continue">
        <a href="/demo/define" class="btn btn-primary rubric_navigate">
            <?php echo __("to_definitions") ?>
        </a>
    </div>
</div>

<script>
    var isDemo = true;
</script>