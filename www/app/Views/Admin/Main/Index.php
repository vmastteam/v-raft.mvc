<div id="tools">
    <ul class="nav nav-tabs">
        <li role="presentation" id="common_tools" class="my_tab active">
            <a href="#"><?php echo __("common_tools") ?></a>
        </li>
        <li role="presentation" id="faq_tools" class="my_tab">
            <a href="#"><?php echo __("faq") ?></a>
        </li>
    </ul>

    <div id="common_tools_content" class="my_content shown">
        <div class="tools_left">
            <div class="update_langs">
                <div class="tools_title"><?php echo __("update_lang_db"); ?></div>
                <button class="btn btn-warning"><?php echo __("go"); ?></button>
                <img src="<?php echo template_url("img/loader.gif") ?>">
            </div>
        </div>

        <div class="tools_right">

        </div>

        <div class="clear"></div>
    </div>

    <div id="faq_tools_content" class="my_content">
        <div class="tools_left" style="width: 45%">
            <div class="faq_create faq_content">
                <div class="form-group">
                    <label for="faq_question" class="sr-only">Question</label>
                    <textarea class="form-control textarea" id="faq_question" placeholder="<?php echo __("faq_enter_question") ?>"></textarea>
                </div>
                <div class="form-group">
                    <label for="faq_answer" class="sr-only">Answer</label>
                    <textarea class="form-control" id="faq_answer" placeholder="<?php echo __("faq_enter_answer") ?>"></textarea>
                </div>
                <div class="form-group">
                    <select class="form-control" id="faq_category" name="category">
                        <option value="" hidden><?php echo __('select_news_category'); ?></option>
                        <option value="common"><?php echo __("common") ?></option>
                        <option value="vraft"><?php echo __("vraft") ?></option>
                    </select>
                </div>
                <button class="btn btn-primary create_faq"><?php echo __("create") ?></button>
                <img id="faq_create_loader" src="<?php echo template_url("img/loader.gif") ?>" style="margin-top: -5px">
            </div>
        </div>

        <div class="tools_right" style="width: 53%">
            <div class="faq_filter tools">
                <div class="form-group">
                    <label for="faqfilter" class="sr-only">Filter</label>
                    <input type="text" class="form-control" id="faqfilter" placeholder="<?php echo __("filter_by_search") ?>" value="">
                </div>
            </div>

            <hr>

            <div class="faq_list tools">
                <ul>
                    <?php foreach ($data["faqs"] as $faq): ?>
                        <li class="faq_content" id="<?php echo $faq->id ?>">
                            <div class="tools_delete_faq">
                                <span><?php echo __("delete") ?></span>
                                <img src="<?php echo template_url("img/loader.gif") ?>">
                            </div>

                            <div class="faq_question"><?php echo $faq->title ?></div>
                            <div class="faq_answer"><?php echo $faq->text ?></div>
                            <div class="faq_cat"><?php echo __($faq->category) ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        autosize($("textarea"));
        $(".my_tab").click(function () {
            autosize($("textarea"));
        });
    });
</script>