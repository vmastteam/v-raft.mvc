<div class="rubric_page">
    <h2><?php echo mb_strtoupper(__("qualities")) ?></h2>

    <div class="rubric_item_container">
        <div class="rubric_item_header">
            <div class="rubric_orig"><?php echo __("original") ?></div>
            <div class="rubric_eng"><?php echo __("eng") ?></div>
            <div class="clear"></div>
        </div>
        <div class="parent_block">
            <div class="rubric_items" data-type="quality" data-parentid="<?php echo $data['lang']->langID ?>">
                <?php if(sizeof($data['qualities']) == 0): ?>
                    <?php for($i=1;$i<=10; $i++): ?>
                    <div class="rubric_item_block">
                        <span><?php echo sprintf('%02d', $i); ?>.</span>
                        <textarea class="rubric_item_orig" rows="2" dir="<?php echo $data['lang']->direction ?>"></textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove" title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2"></textarea>
                    </div>
                    <?php endfor; ?>
                <?php else: ?>
                    <?php $i=1; foreach($data['qualities'] as $quality): ?>
                    <div class="rubric_item_block" data-id="<?php echo $quality->id ?>">
                        <span><?php echo sprintf('%02d', $i); ?>.</span>
                        <textarea class="rubric_item_orig" rows="2" dir="<?php echo $data['lang']->direction ?>"><?php echo $quality->content ?></textarea>
                        <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove" title="<?php echo __('delete') ?>"></button>
                        <textarea class="rubric_item_eng" rows="2"><?php echo $quality->eng ?></textarea>
                    </div>  
                    <?php $i++; endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="rubric_item_add">
                <button class="btn btn-success glyphicon glyphicon-plus" data-dir="<?php echo $data['lang']->direction ?>" title="<?php echo __('add') ?>"></button>
            </div>
        </div>
        <hr>
    </div>

    <div class="rubric_item_continue">
        <a href="/rubric/<?php echo $data['lang']->langID ?>/define" class="btn btn-primary rubric_navigate"><?php echo __("to_definitions") ?></a>
    </div>
</div>
