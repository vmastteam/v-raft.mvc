<?php
use Shared\Legacy\Error;
?>

<div class="members_login">
    <h1><?php echo __('passwordreset_title'); ?></h1>

    <?php
    echo Error::display($error);
    ?>

    <form action='' method='post' style="width: 500px">
        <div class="form-group">
            <label for="email"><?php echo __('enter_email') ?></label>
            <input type="text" class="form-control" id="email" name="email"
                   placeholder="<?php echo __('enter_email') ?>" value="">
        </div>

        <input type="hidden" name="csrfToken" value="<?php echo $data['csrfToken']; ?>"/>

        <?php if(Config::get("app.type") == "remote"): ?>
        <button class="g-recaptcha btn btn-primary btn-lg" data-sitekey="<?php echo ReCaptcha::getSiteKey() ?>" data-callback='onSubmit'><?php echo __('continue'); ?></button>
        <?php else: ?>
        <button type="submit" class="btn btn-primary btn-lg"><?php echo __('continue'); ?></button>
        <?php endif; ?>
    </form>
</div>

<script type="text/javascript">
	function onSubmit(token) {
		$("form")[0].submit();
	}
</script>

<?php if(Config::get("app.type") == "remote"): ?>
<script src="https://www.google.com/recaptcha/api.js?hl=<?php echo Language::code()?>" async defer></script>
<?php endif; ?>