<div id="rubric_bg">
    <div id="rubric_left">
        <div class="event_link left_elm"><a href="/create"> <?php echo __("edit_rubric") ?> </a></div>
        <div class="event_link rubric_list_link left_elm"><a href="/rubric/all"> <?php echo __("view_rubrics") ?> </a></div>
        <div class="event_link rubric_demo_link left_elm"><a href="/demo"> <?php echo __("demo") ?> </a></div>
    </div>
    <div id="rubric_right">
        <div class="right_elm"><?php echo __("edit_rubric_desc") ?></div>
        <div class="right_elm"><?php echo __("view_rubrics_desc") ?></div>
        <div class="right_elm"><?php echo __("demo_rubric_desc") ?></div>
    </div>
</div>