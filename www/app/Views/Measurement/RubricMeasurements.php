<div class="rubric_page">
    <h2><?php echo mb_strtoupper(__("measurements")) ?></h2>

    <div class="rubric_item_container">
        <div class="rubric_item_header">
            <div class="rubric_orig"><?php echo __("original") ?></div>
            <div class="rubric_eng"><?php echo __("eng") ?></div>
            <div class="clear"></div>
        </div>
        <div class="rubric_qualities">
        <?php $tr=1; foreach($data["qualities"] as $quality): ?>
            <h3 class="level1" dir="<?php echo $data['lang']->direction ?>">
                <?php echo sprintf('%02d', $tr); ?>. <?php echo $quality->content; ?>
            </h3>
            <h3 class="level1 right"><?php echo $quality->eng; ?></h3>
            <div class="level1 rubric_defs">
                <?php $df=1; foreach($quality->defs as $def): ?>
                <h3 class="level2" dir="<?php echo $data['lang']->direction ?>">
                    <?php echo sprintf('%02d', $df); ?>. <?php echo $def->content; ?>
                </h3>
                <h3 class="level2 right"><?php echo $def->eng; ?></h3>
                <div class="level2 parent_block">
                    <div class="rubric_items" data-type="measurement" data-parentid="<?php echo $def->id ?>">
                        <?php if(sizeof($def->measurements) == 0): ?>
                            <?php for($i=1;$i<=1; $i++): ?>
                            <div class="rubric_item_block">
                                <span><?php echo sprintf('%02d', $i); ?>.</span>
                                <textarea class="rubric_item_orig" rows="2" dir="<?php echo $data['lang']->direction ?>"></textarea>
                                <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove" title="<?php echo __('delete') ?>"></button>
                                <textarea class="rubric_item_eng" rows="2"></textarea>
                            </div>
                            <?php endfor; ?>
                        <?php else: ?>
                            <?php $i=1; foreach($def->measurements as $measurement): ?>
                            <div class="rubric_item_block" data-id="<?php echo $measurement->id ?>">
                                <span><?php echo sprintf('%02d', $i); ?>.</span>
                                <textarea class="rubric_item_orig" rows="2" dir="<?php echo $data['lang']->direction ?>"><?php echo $measurement->content ?></textarea>
                                <button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove" title="<?php echo __('delete') ?>"></button>
                                <textarea class="rubric_item_eng" rows="2"><?php echo $measurement->eng ?></textarea>
                            </div>  
                            <?php $i++; endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="rubric_item_add">
                        <button class="btn btn-success glyphicon glyphicon-plus" data-dir="<?php echo $data['lang']->direction ?>" title="<?php echo __('add') ?>"></button>
                    </div>
                </div>
                <hr>
                <?php $df++; endforeach; ?>
            </div>
        <?php $tr++; endforeach; ?>
        </div>
    </div>

    <div class="rubric_item_continue">
        <a href="/rubric/<?php echo $data['lang']->langID ?>/define" class="btn btn-primary rubric_navigate">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <?php echo __("to_definitions") ?>
        </a> 
        <a href="/rubric/<?php echo $data['lang']->langID ?>" class="btn btn-primary rubric_navigate">
            <?php echo __("finish") ?>
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
