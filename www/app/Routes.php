<?php
/**
 * Routes - all standard Routes are defined here.
 *
 * @author David Carr - dave@daveismyname.com
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */

/** Define static routes. */

// The default Routing (MAIN)
Route::group(["prefix" => "", "namespace" => "App\Controllers"], function() {
    Router::any("", "MainController@index");
    Router::any("create", "MainController@create");
    Router::get("about", "MainController@about");
    Router::get("contact", "MainController@contactUs");
    Router::get("rpc/check_internet", "MainController@contactUs");
    Router::any("passwordreset/{lang}", "MainController@passwordReset");
    Router::any("passwordreset/{lang}/{resetToken}", "MainController@passwordResetToken");
    Router::any("success", "MainController@success");
    Router::any("logout", "MainController@logout");

    // Rubric API endpoint
    Router::any("api/rubric/{lang}", "RubricController@readRubricApi");
});

// DEMO
Route::group(["prefix" => "demo", "namespace" => "App\Controllers"], function() {
    Router::any("{page?}", "DemoController@index");
    Router::any("download/{type}/{cjk?}/", "DemoController@download");
});

// FAQ
Route::group(["prefix" => "faq", "namespace" => "App\Controllers"], function() {
    Router::any("", "FaqController@index");
});

// RUBRIC
Route::group(["prefix" => "rubric", "namespace" => "App\Controllers"], function() {
    Router::get("", "RubricController@index");
    Router::get("all", "RubricController@rubrics");
    Router::get("{lang}", "RubricController@readRubric");
    Router::get("{lang}/download/{type}/{cjk?}", "RubricController@downloadRubric");
    Router::any("test/{lang}", "RubricController@testRubric");
});

// QUALITY
Route::group(["prefix" => "rubric", "namespace" => "App\Controllers"], function() {
    Router::get("{lang}/qualities", "QualityController@index");
    Router::delete("quality/{id}", "QualityController@deleteQuality")
        ->where(["id" => "[0-9]+"]);
    Router::post("quality/{lang}", "QualityController@createQuality")
        ->where(["rubricID" => "[0-9]+"]);
    Router::put("quality/{id}", "QualityController@updateQuality")
        ->where(["id" => "[0-9]+"]);
});

// DEF
Route::group(["prefix" => "rubric", "namespace" => "App\Controllers"], function() {
    Router::get("{lang}/define", "DefController@index");
    Router::delete("def/{id}", "DefController@deleteDef")
        ->where(["id" => "[0-9]+"]);
    Router::post("def/{qualityID}", "DefController@createDef")
        ->where(["qualityID" => "[0-9]+"]);
    Router::put("def/{id}", "DefController@updateDef")
        ->where(["id" => "[0-9]+"]);
});

// MEASUREMENT
Route::group(["prefix" => "rubric", "namespace" => "App\Controllers"], function() {
    Router::get("{lang}/measurements", "MeasurementController@index");
    Router::delete("measurement/{id}", "MeasurementController@deleteMeasurement")
        ->where(["id" => "[0-9]+"]);
    Router::post("measurement/{defID}", "MeasurementController@createMeasurement")
        ->where(["defID" => "[0-9]+"]);
    Router::put("measurement/{id}", "MeasurementController@updateMeasurement")
        ->where(["id" => "[0-9]+"]);
});

// ADMIN
Route::group(["prefix" => "admin", "namespace" => "App\Controllers\Admin"], function() {
    Router::any("", "AdminController@index");
    Router::any("languages/update", "AdminController@updateLanguages");
    Router::any("faq/delete", "AdminController@deleteFaq");
    Router::any("faq/create", "AdminController@createFaq");
    Router::any("image/upload", "AdminController@uploadImage");
});

/** End default Routes */
