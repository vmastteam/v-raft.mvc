<?php


namespace App\Providers;


use Support\ServiceProvider;

class LanguageServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Language\ILanguageRepository",
            "App\Repositories\Language\LanguageRepository");
    }

}