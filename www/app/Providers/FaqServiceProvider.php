<?php


namespace App\Providers;


use Support\ServiceProvider;

class FaqServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Faq\IFaqRepository",
            "App\Repositories\Faq\FaqRepository");
    }

}