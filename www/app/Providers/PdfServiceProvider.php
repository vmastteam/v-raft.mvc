<?php


namespace App\Providers;


use Support\ServiceProvider;

class PdfServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Pdf\IPdfRepository",
            "App\Repositories\Pdf\PdfRepository");
    }

}