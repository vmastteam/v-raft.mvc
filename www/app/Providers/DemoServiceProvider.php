<?php


namespace App\Providers;


use Support\ServiceProvider;

class DemoServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Demo\IDemoRepository",
            "App\Repositories\Demo\DemoRepository");
    }

}