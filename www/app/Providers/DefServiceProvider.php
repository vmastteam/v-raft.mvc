<?php


namespace App\Providers;


use Support\ServiceProvider;

class DefServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Def\IDefRepository",
            "App\Repositories\Def\DefRepository");
    }

}