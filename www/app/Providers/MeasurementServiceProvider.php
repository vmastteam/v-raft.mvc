<?php


namespace App\Providers;


use Support\ServiceProvider;

class MeasurementServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Measurement\IMeasurementRepository",
            "App\Repositories\Measurement\MeasurementRepository");
    }

}