<?php


namespace App\Providers;


use Support\ServiceProvider;

class QualityServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Quality\IQualityRepository",
            "App\Repositories\Quality\QualityRepository");
    }

}