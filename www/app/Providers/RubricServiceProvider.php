<?php


namespace App\Providers;


use Support\ServiceProvider;

class RubricServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind("App\Repositories\Rubric\IRubricRepository",
            "App\Repositories\Rubric\RubricRepository");
    }

}