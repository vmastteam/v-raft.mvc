<?php
namespace App\Models;

use Database\ORM\Model;

class Measurement extends Model
{
    protected $fillable = array("content", "eng");

    public function def()
    {
        return $this->belongsTo(Def::class);
    }
}