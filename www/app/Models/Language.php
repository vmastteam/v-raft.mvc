<?php
namespace App\Models;

use Database\ORM\Model;

class Language extends Model
{
   public $timestamps  = false;
   protected $primaryKey = 'langID';
}