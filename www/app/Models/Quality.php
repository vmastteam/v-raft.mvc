<?php
namespace App\Models;

use Database\ORM\Model;

class Quality extends Model
{
    protected $fillable = array("content", "eng");

    public function defs()
    {
        return $this->hasMany(Def::class, "quality_id");
    }

    public function rubric()
    {
        return $this->belongsTo(Rubric::class);
    }
}
