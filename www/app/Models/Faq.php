<?php
namespace App\Models;

use Database\ORM\Model;

class Faq extends Model
{
    protected $fillable = array("category", "title", "text", "lang");
}