<?php
namespace App\Models;

use Database\ORM\Model;

class Rubric extends Model
{
    protected $fillable = array('lang', 'password', 'email');
    
    public function qualities()
    {
        return $this->hasMany(Quality::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'lang');
    }
}
