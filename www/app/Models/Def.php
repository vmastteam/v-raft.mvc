<?php
namespace App\Models;

use Database\ORM\Model;

class Def extends Model
{
    protected $fillable = array("content", "eng");

    public function measurements()
    {
        return $this->hasMany(Measurement::class);
    }

    public function quality()
    {
        return $this->belongsTo(Quality::class);
    }
}
