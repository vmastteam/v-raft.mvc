<?php
namespace App\Controllers;

use App\Repositories\Language\ILanguageRepository;
use App\Repositories\Rubric\IRubricRepository;
use View;
use App\Core\Controller;
use Helpers\Session;
use Helpers\Url;
use Helpers\Csrf;
use Input;
use Helpers\Password;
use Helpers\ReCaptcha;
use Config\Config;
use Cookie;
use Mailer;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class MainController extends Controller
{
    protected $language = null;
    protected $rubric = null;

    /**
     * Call the parent construct
     * @param ILanguageRepository $language
     * @param IRubricRepository $rubric
     */
    public function __construct(ILanguageRepository $language, IRubricRepository $rubric)
    {
        parent::__construct();

        $this->language = $language;
        $this->rubric = $rubric;
    }

    public function before()
    {
        return parent::before();
    }

    public function index()
    {
        if(Session::get('loggedin'))
        {
            if(Session::get('isAdmin'))
                Url::redirect("/admin");
            else
                Url::redirect("/rubric/".Session::get("lang")."/qualities");
        }

        $data['menu'] = 1;
        $data["isMain"] = true;

        return View::make('Main/Index')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }


    /**
     * Define Create page title and load template files
     */
    public function create()
    {
        if(Session::get('loggedin'))
        {
            if(Session::get('isAdmin'))
                Url::redirect("/admin");
            else
                Url::redirect("/rubric/".Session::get("lang")."/qualities");
        }

        $data['menu'] = 1;
        $data["isMain"] = true;
        $data["languages"] = $this->language->all();
        $data['csrfToken'] = Csrf::makeToken();

        if(!empty($_POST))
        {
            if (!Csrf::isTokenValid())
            {
                Url::redirect('/');
            }
            
            $input = Input::only('type', 'lang', 'password', 
                'email', 'tou', 'sof', 'passwordConfirm', 'loginPassword');
        
            foreach ($input as $key => $value)
                $input[$key] = trim(strip_tags($value));
            
            if($input['type'] == 'create')
            {
                $lang = $this->language->get($input['lang']);
                
                if(!$lang)
                {
                    $error['lang'] = __('wrong_language_error');
                }
                
                if (!filter_var($input['email'], FILTER_VALIDATE_EMAIL))
                {
                    $error['email'] = __('enter_valid_email_error');
                }

                if (strlen($input['password']) < 5)
                {
                    $error['password'] = __('password_short_error');
                }
                elseif ($input['password'] != $input['passwordConfirm'])
                {
                    $error['passwordConfirm'] = __('passwords_notmatch_error');
                }

                if(Config::get("app.type") == "remote")
                {
                    if (!ReCaptcha::check())
                    {
                        $error['recaptcha'] = __('captcha_wrong');
                    }
                }

                if(!$input['tou'])
                {
                    $error['tou'] = __('tou_accept_error');
                }

                if(!$input['sof'])
                {
                    $error['sof'] = __('sof_accept_error');
                }
                
                if (!isset($error))
                {
                    $exist = $this->rubric->getByLang($input['lang']);

                    if($exist)
                    {
                        $error['langid'] = __('rubric_exist_error');
                    }
                    else
                    {
                        $rubric = $this->rubric->create([
                            'lang' => $input['lang'],
                            'email' => $input['email'],
                            'password' => Password::make($input['password'])
                        ]);

                        if($rubric)
                        {
                            $lang = $rubric->language;
                            
                            Session::set('loggedin', true);
                            Session::set('email', $input['email']);
                            Session::set('lang', $input['lang']);
                            Session::set('langname', $lang->langName);
                            Session::set('isAdmin', $rubric->isAdmin);

                            // Save cookie for week
                            Cookie::queue(PREFIX .'lastlang', $lang->langID, Cookie::FIVEYEARS);

                            if($rubric->isAdmin)
                                Url::redirect('/admin');
                            else
                                Url::redirect('/rubric/'.$lang->langID.'/qualities');
                        }
                    }
                }
            }
            else if($input['type'] == 'login')
            {
                $rubric = $this->rubric->getByLangWith($input['lang'], "language");
                
                if($rubric)
                {
                    if(Password::verify($input['loginPassword'], $rubric->password))
                    {
                        $lang = $rubric->language;

                        Session::set('loggedin', true);
                        Session::set('email', $rubric->email);
                        Session::set('lang', $lang->langID);
                        Session::set('langname', $lang->langName);
                        Session::set('isAdmin', $rubric->isAdmin);

                        // Save cookie for week
                        Cookie::queue(PREFIX .'lastlang', $lang->langID, Cookie::FIVEYEARS);

                        if($rubric->isAdmin)
                            Url::redirect('/admin');
                        else
                            Url::redirect('/rubric/'.$lang->langID.'/qualities');
                    }
                    else
                    {
                        $error['loginPassword'] = __('wrong_password_error');
                    }
                }
                else
                {
                    $error['langid'] = __('rubric_not_exist_error');
                }
            }
        }

        return View::make('Main/Create')
            ->shares("title", __("welcome_text"))
            ->shares("error", @$error)
            ->shares("data", $data);
    }


    public function passwordReset($lang)
    {
        if(Session::get('loggedin'))
        {
            if(Session::get('isAdmin'))
                Url::redirect("/admin");

            if(Session::get('lang'))
            {
                Url::redirect("rubric/" . Session::get('lang') . "/qualities");
            }
            else
            {
                Url::redirect("rubric/");
            }
        }

        $data['menu'] = 1;
        $data["isMain"] = true;
        $data['csrfToken'] = Csrf::makeToken();

        if(!empty($_POST))
        {
            if (!Csrf::isTokenValid())
            {
                Url::redirect('passwordreset');
            }

            $input = Input::only('email');
    
            foreach ($input as $key => $value) {
                $input[$key] = trim(strip_tags($value));
            }

            if (!filter_var($input['email'], FILTER_VALIDATE_EMAIL))
            {
                $error['email'] = __('enter_valid_email_error');
            }

            if(Config::get("app.type") == "remote")
            {
                if (!ReCaptcha::check())
                {
                    $error[] = __('captcha_wrong');
                }
            }

            if(!isset($error))
            {
                $rubric = $this->rubric->getByEmail($lang, $input['email']);
                
                if($rubric)
                {
                    $resetToken = md5(uniqid(rand(), true));

                    $rubric->resetToken = $resetToken;
                    $rubric->resetDate = date('Y-m-d H:i:s',time());
                    $this->rubric->save($rubric);

                    if(Config::get("app.type") == "remote")
                    {
                        Mailer::send('Emails/Auth/PasswordResetRubric', ["lang" => $lang, "token" => $resetToken], function($message) use($rubric)
                        {
                            $message->to($rubric->email, $rubric->email)
                                ->subject(__('passwordreset_title'));
                        });
                        $msg = __('pwresettoken_send_success');
                    }
                    else
                    {
                        $msg = __("passwordreset_link_message", '<a href="'.site_url('passwordreset/'.$lang.'/'.$resetToken).'">'.site_url('passwordreset/'.$lang.'/'.$resetToken).'</a>');
                    }

                    Session::set('success', $msg);

                    Url::redirect('success');
                }
                else
                {
                    $error[] = __('enter_valid_email_error');
                }
            }
        }

        return View::make('Main/PasswordReset')
            ->shares("title", __("password_reset_title"))
            ->shares("data", $data)
            ->shares("error", @$error);
    }

    public function passwordResetToken($lang, $resetToken)
    {
        if(Session::get('loggedin'))
        {
            if(Session::get('isAdmin'))
                Url::redirect("/admin");

            if(Session::get('lang'))
            {
                Url::redirect("rubric/" . Session::get('lang') . "/qualities");
            }
            else
            {
                Url::redirect("rubric/");
            }
        }

        $data['step'] = 1;
        $data['menu'] = 1;

        if ((strlen($resetToken) == 32))
        {
            $rubric = $this->rubric->getByResetToken($lang, $resetToken);

            if (!$rubric)
            {
                $error[] = __('rubric_not_exist_error');
            }
            elseif((time() - strtotime($rubric->resetDate) > (60*60*12))) // 12 hours expiration
            {
                $error[] = __('token_expired_error');
                $rubric->resetToken = null;
                $this->rubric->save($rubric);
            }
            else
            {
                $data['step'] = 2;

                if(!empty($_POST))
                {
                    if (!Csrf::isTokenValid())
                    {
                        Url::redirect("/resetpassword/$resetToken");
                    }

                    $input = Input::only('password', 'passwordConfirm');
                    
                    foreach ($input as $key => $value) {
                        $input[$key] = trim(strip_tags($value));
                    }

                    $password = $input['password'];
                    $passwordConfirm = $input['passwordConfirm'];

                    if (strlen($password) < 5)
                    {
                        $error[] = __('password_short_error');
                    }
                    elseif ($password != $passwordConfirm)
                    {
                        $error[] = __('passwords_notmatch_error');
                    }

                    if (!isset($error))
                    {
                        $rubric->password = Password::make($password);
                        $rubric->resetToken = null;
                        $this->rubric->save($rubric);
                        
                        $msg = __('password_reset_success', [SITEURL]);
                        Session::set('success', $msg);

                        Url::redirect('success');
                    }
                }
            }
        }
        else
        {
            $error[] = __('invalid_link_error');
        }

        $data['csrfToken'] = Csrf::makeToken();

        return View::make('Main/ResetPassword')
            ->shares("title", __("passwordreset_title"))
            ->shares("data", $data)
            ->shares("error", @$error);
    }

    /**
     * Logout user
     * @return mixed
     */
    public function logout()
    {
        Session::destroy();
        Url::redirect('/', true);
    }

    /**
     * Show success veiw
     * @return mixed
     */
     public function success()
     {
         return View::make('Main/Success')
             ->shares("title", __("success"));
     }

    /**
     * About Us Page
     */
    public function about()
    {
        $data['title'] = __('about_title');
        $data['menu'] = 6;

        return View::make('Main/About')
            ->shares("title", __("about_title"))
            ->shares("data", $data);
    }

    /**
     * Contact Us page
     */
    public function contactUs()
    {
        $data['menu'] = 5;

        return View::make('Main/ContactUs')
            ->shares("title", __("contact_us_title"))
            ->shares("data", $data);
    }

    /**
     * Check internet connection
     */
    public function checkInternet()
    {
        return time();
    }
}
