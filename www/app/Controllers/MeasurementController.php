<?php
namespace App\Controllers;

use App\Repositories\Def\IDefRepository;
use App\Repositories\Language\ILanguageRepository;
use App\Repositories\Measurement\IMeasurementRepository;
use App\Repositories\Quality\IQualityRepository;
use Helpers\Url;
use View;
use App\Core\Controller;
use Helpers\Session;
use Response;
use Input;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class MeasurementController extends Controller
{
    protected $measurement = null;
    protected $def = null;
    protected $quality = null;
    protected $language = null;

    /**
     * Call the parent construct
     * @param IMeasurementRepository $measurement
     * @param IDefRepository $def
     * @param IQualityRepository $quality
     * @param ILanguageRepository $language
     */
    public function __construct(IMeasurementRepository $measurement,
                                IDefRepository $def,
                                IQualityRepository $quality,
                                ILanguageRepository $language)
    {
        parent::__construct();

        $this->measurement = $measurement;
        $this->def = $def;
        $this->quality = $quality;
        $this->language = $language;
    }

    public function before()
    {
        return parent::before();
    }

    public function index($lang)
    {
        if(!Session::get('loggedin'))
        {
            Url::redirect("/");
        }
        if(Session::get('isAdmin'))
        {
            Url::redirect("/admin");
        }
        if(Session::get("lang") != $lang)
        {
            Url::redirect("/");
        }

        $data['qualities'] = $this->quality->getByLangWith($lang, ["defs.measurements"]);
        $data['lang'] = $this->language->get($lang);
        $data['menu'] = 1;

        return View::make('Measurement/RubricMeasurements')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }


    /**
     * Delete measurement
     * @param $id ID of a measurement
     * @return json response
     * @throws \Exception
     */
    public function deleteMeasurement($id)
    {
        $response = ["error" => __("unknown_error")];
        $measurement = $this->measurement->getWith($id, "def.quality.rubric");
        if($measurement)
        {
            if($measurement->def->quality->rubric->lang == Session::get("lang"))
            {
                if($this->measurement->delete($measurement))
                {
                    $response = ["success" => true];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_measurement_error")];
        }

        return Response::json($response);
    }

    /**
     * Create new measurement for specified def
     * @param $defID
     * @return json response
     */
    public function createMeasurement($defID)
    {
        $response = ["error" => __("unknown_error")];

        $input = Input::only('orig', 'eng');
        foreach ($input as $key => $value)
            $input[$key] = trim(strip_tags($value));

        $def = $this->def->getWith($defID, "quality.rubric");

        if($def)
        {
            if($def->quality->rubric->lang == Session::get("lang"))
            {
                $measurement = $this->measurement->create([
                    "content" => $input["orig"],
                    "eng" => $input["eng"]
                ], $def);

                if($this->measurement->save($measurement))
                {
                    $response = [
                        "success" => true,
                        "id" => $measurement->id
                    ];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("wrong_def_error")];
        }

        return Response::json($response);
    }

    /**
     * Update measurement
     * @param $id
     * @return json response
     */
    public function updateMeasurement($id)
    {
        $response = ["error" => __("unknown_error")];
        $measurement = $this->measurement->getWith($id, "def.quality.rubric");
        if($measurement)
        {
            if($measurement->def->quality->rubric->lang == Session::get("lang"))
            {
                $input = Input::only('orig', 'eng');
                foreach ($input as $key => $value)
                    $input[$key] = trim(strip_tags($value));
                $measurement->content = $input["orig"];
                $measurement->eng = $input["eng"];

                if($this->measurement->save($measurement))
                {
                    $response = ["success" => true];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_Measurement_error")];
        }

        return Response::json($response);
    }
}
