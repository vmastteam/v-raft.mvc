<?php
namespace App\Controllers;

use App\Repositories\Faq\IFaqRepository;
use View;
use App\Core\Controller;
use Response;
use Input;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class FaqController extends Controller
{
    protected $faq = null;

    /**
     * Call the parent construct
     * @param IFaqRepository $faq
     */
    public function __construct(IFaqRepository $faq)
    {
        parent::__construct();

        $this->faq = $faq;
    }

    public function before()
    {
        return parent::before();
    }


    /**
     * Define Index page title and load template files
     * @return View
     */
    public function index()
    {
        $data['menu'] = 4;
        $data["faqs"] = $this->faq->all();

        return View::make('Faq/Index')
            ->shares("title", __("faq_title"))
            ->shares("data", $data);
    }

    public function download($type, $cjk = false)
    {
        $rubric = $this->demo->getRubric();
        $this->pdf->download($rubric, $type, $cjk);
    }
}
