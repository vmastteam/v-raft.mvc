<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Repositories\Pdf\IPdfRepository;
use App\Repositories\Rubric\IRubricRepository;
use Helpers\Session;
use Helpers\Url;
use Input;
use View;
use Response;
use TCPDF;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class RubricController extends Controller
{
    protected $rubric = null;
    protected $pdf = null;

    /**
     * Call the parent construct
     * @param IRubricRepository $rubric
     * @param IPdfRepository $pdf
     */
    public function __construct(IRubricRepository $rubric, IPdfRepository $pdf)
    {
        parent::__construct();

        $this->rubric = $rubric;
        $this->pdf = $pdf;
    }

    public function before()
    {
        return parent::before();
    }

    /**
     * Define Index page title and load template files
     */
    public function index()
    {
        if(!Session::get('loggedin'))
        {
            Url::redirect("/");
        }
        else
        {
            Url::redirect('rubric/'.Session::get("lang").'/qualities');
        }
    }

    public function rubrics()
    {
        $data['menu'] = 2;
        $rubrics = $this->rubric->all();

        $data['rubrics'] = $rubrics;

        return View::make('Rubric/Rubrics')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }


    public function rubricDefs($lang)
    {
        if(!Session::get('loggedin'))
        {
            Url::redirect("/");
        }
        if(Session::get('isAdmin'))
        {
            Url::redirect("/admin");
        }
        if(Session::get("lang") != $lang)
        {
            Url::redirect("/");
        }

        $rubric = $this->rubric->getByLang($lang);
        if( $rubric)
        {
            $data['rubric'] = $rubric;
        }

        $data['menu'] = 1;

        return View::make('Rubric/RubricDefs')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }

    public function rubricMeasurements($lang)
    {
        if(!Session::get('loggedin'))
        {
            Url::redirect("/");
        }
        if(Session::get('isAdmin'))
        {
            Url::redirect("/admin");
        }
        if(Session::get("lang") != $lang)
        {
            Url::redirect("/");
        }

        $rubric = $this->rubric->getByLang($lang);
        if($rubric)
        {
            $data['rubric'] = $rubric;
        }
        
        $data['menu'] = 1;

        return View::make('Rubric/RubricMeasurements')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }

    public function readRubric($lang)
    {
        $rubric = $this->rubric->getByLangWith($lang, "qualities.defs.measurements");

        if($rubric)
        {
            $data['rubric'] = $rubric;
        }
        $data['menu'] = 2;

        return View::make('Rubric/ReadRubric')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }

    public function readRubricApi($lang)
    {
        $rubric = $this->rubric->getByLangWith($lang, "qualities.defs.measurements");

        $json = array();

        if($rubric)
        {
            $json["language"] = $rubric->language;
            $json["qualities"] = array();

            foreach ($rubric->qualities as $quality) {
                $defs = array();

                $q = array();
                $q["content"] = $quality->content;
                $q["eng"] = $quality->eng;

                foreach ($quality->defs as $def) {
                    $measurements = array();

                    $d = array();
                    $d["content"] = $def->content;
                    $d["eng"] = $def->eng;

                    foreach ($def->measurements as $measurement) {
                        $m = array();
                        $m["content"] = $measurement->content;
                        $m["eng"] = $measurement->eng;

                        $measurements[] = $m;
                    }

                    $d["measurements"] = $measurements;
                    $defs[] = $d;
                }

                $q["defs"] = $defs;
                $json["qualities"][] = $q;
            }
        }

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($json, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
        exit;
    }

    public function downloadRubric($lang, $type, $cjk = false)
    {
        $rubric = $this->rubric->getByLangWith($lang, "qualities.defs.measurements");

        if(!$rubric)
        {
            echo "Error. Rubric does not exist";
            exit;
        }

        $this->pdf->download($rubric, $type, $cjk);
    }

    public function testRubric($lang)
    {
        $rubric = $this->rubric->getByLang($lang);
        $response = ['rubric' => true];
        if(!$rubric)
        {
            $response['rubric'] = false;
        }

        return Response::json($response);
    }
}
