<?php
namespace App\Controllers;

use App\Repositories\Def\IDefRepository;
use App\Repositories\Language\ILanguageRepository;
use App\Repositories\Quality\IQualityRepository;
use Helpers\Url;
use View;
use App\Core\Controller;
use Helpers\Session;
use Response;
use Input;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class DefController extends Controller
{
    protected $def = null;
    protected $quality = null;
    protected $language = null;

    /**
     * Call the parent construct
     * @param IDefRepository $def
     * @param IQualityRepository $quality
     * @param ILanguageRepository $language
     */
    public function __construct(IDefRepository $def,
                                IQualityRepository $quality,
                                ILanguageRepository $language)
    {
        parent::__construct();

        $this->def = $def;
        $this->quality = $quality;
        $this->language = $language;
    }

    public function before()
    {
        return parent::before();
    }

    public function index($lang)
    {
        if(!Session::get('loggedin'))
        {
            Url::redirect("/");
        }
        if(Session::get('isAdmin'))
        {
            Url::redirect("/admin");
        }
        if(Session::get("lang") != $lang)
        {
            Url::redirect("/");
        }

        $data['qualities'] = $this->quality->getByLang($lang);
        $data['lang'] = $this->language->get($lang);
        $data['menu'] = 1;

        return View::make('Def/RubricDefs')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }


    /**
     * Delete def
     * @param $id ID of a def
     * @return json response
     * @throws \Exception
     */
    public function deleteDef($id)
    {
        $response = ["error" => __("unknown_error")];
        $def = $this->def->getWith($id, "quality.rubric");
        if($def)
        {
            if($def->quality->rubric->lang == Session::get("lang"))
            {
                if($this->def->delete($def))
                {
                    $response = ["success" => true];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_def_error")];
        }

        return Response::json($response);
    }

    /**
     * Create new def for specified quality
     * @param $qualityID
     * @return json response
     */
    public function createDef($qualityID)
    {
        $response = ["error" => __("unknown_error")];

        $input = Input::only('orig', 'eng');
        foreach ($input as $key => $value)
            $input[$key] = trim(strip_tags($value));

        $quality = $this->quality->getWith($qualityID, "rubric");
        if($quality)
        {
            if($quality->rubric->lang == Session::get("lang"))
            {
                $def = $this->def->create([
                    "content" => $input["orig"],
                    "eng" => $input["eng"]
                ], $quality);

                if($this->def->save($def))
                {
                    $response = [
                        "success" => true,
                        "id" => $def->id
                    ];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_quality_error")];
        }

        return Response::json($response);
    }

    /**
     * Update def
     * @param $id
     * @return json response
     */
    public function updateDef($id)
    {
        $response = ["error" => __("unknown_error")];
        $def = $this->def->getWith($id, "quality.rubric");
        if($def)
        {
            if($def->quality->rubric->lang == Session::get("lang"))
            {
                $input = Input::only('orig', 'eng');
                foreach ($input as $key => $value)
                    $input[$key] = trim(strip_tags($value));
                $def->content = $input["orig"];
                $def->eng = $input["eng"];

                if($this->def->save($def))
                {
                    $response = ["success" => true];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_def_error")];
        }

        return Response::json($response);
    }
}
