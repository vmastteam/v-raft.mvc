<?php
namespace App\Controllers;

use App\Repositories\Language\ILanguageRepository;
use App\Repositories\Quality\IQualityRepository;
use App\Repositories\Rubric\IRubricRepository;
use Helpers\Url;
use View;
use App\Core\Controller;
use Helpers\Session;
use Response;
use Input;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class QualityController extends Controller
{
    protected $quality = null;
    protected $rubric = null;
    protected $language = null;

    /**
     * Call the parent construct
     * @param IQualityRepository $quality
     * @param IRubricRepository $rubric
     * @param ILanguageRepository $language
     */
    public function __construct(IQualityRepository $quality,
                                IRubricRepository $rubric,
                                ILanguageRepository $language)
    {
        parent::__construct();

        $this->quality = $quality;
        $this->rubric = $rubric;
        $this->language = $language;
    }

    public function before()
    {
        return parent::before();
    }

    public function index($lang)
    {
        if(!Session::get('loggedin'))
        {
            Url::redirect("/");
        }
        if(Session::get('isAdmin'))
        {
            Url::redirect("/admin");
        }
        if(Session::get("lang") != $lang)
        {
            Url::redirect("/");
        }

        $data['qualities'] = $this->quality->getByLang($lang);
        $data['lang'] = $this->language->get($lang);
        $data['menu'] = 1;

        return View::make('Quality/RubricQualities')
            ->shares("title", __("rubric_title"))
            ->shares("data", $data);
    }

    /**
     * Delete quality
     * @param $id id of a quality
     * @return json response
     * @throws \Exception
     */
    public function deleteQuality($id)
    {
        $response = ["error" => __("unknown_error")];
        $quality = $this->quality->getWith($id, "rubric");
        if($quality)
        {
            if($quality->rubric && $quality->rubric->lang == Session::get("lang"))
            {
                if($this->quality->delete($quality))
                {
                    $response = ["success" => true];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_quality_error")];
        }

        return Response::json($response);
    }

    /**
    * Create new quality for specified rubric
    * @param $lang
    * @return json response
    */
    public function createQuality($lang)
    {
        $response = ["error" => __("unknown_error")];
        
        $input = Input::only('orig', 'eng');
        foreach ($input as $key => $value)
            $input[$key] = trim(strip_tags($value));

        $rubric = $this->rubric->getByLang($lang);
        if($rubric && $rubric->lang == Session::get("lang"))
        {
            $quality = $this->quality->create([
                "content" => $input["orig"],
                "eng" => $input["eng"]
            ], $rubric);

            if($this->quality->save($quality))
            {
                $response = [
                    "success" => true,
                    "id" => $quality->id
                ];
            }
        }
        else
        {
            $response = ["error" => __("wrong_rubric_error")];
        }

        return Response::json($response);
    }

    /**
    * Update quality
    * @param $id
    * @return json response
    */
    public function updateQuality($id)
    {
        $response = ["error" => __("unknown_error")];
        $quality = $this->quality->getWith($id, "rubric");
        if($quality)
        {
            $rubric = $quality->rubric;
            if($rubric && $rubric->lang == Session::get("lang"))
            {
                $input = Input::only('orig', 'eng');
                foreach ($input as $key => $value)
                    $input[$key] = trim(strip_tags($value));
                $quality->content = $input["orig"];
                $quality->eng = $input["eng"];
                
                if($this->quality->save($quality))
                {
                    $response = ["success" => true];
                }
            }
            else
            {
                $response = ["error" => __("wrong_rubric_error")];
            }
        }
        else
        {
            $response = ["error" => __("no_quality_error")];
        }

        return Response::json($response);
    }
}
