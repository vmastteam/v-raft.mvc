<?php
namespace App\Controllers\Admin;

use App\Core\Controller;
use App\Repositories\Faq\IFaqRepository;
use App\Repositories\Language\ILanguageRepository;
use App\Repositories\Rubric\IRubricRepository;
use Support\Facades\File;
use Support\Facades\Input;
use View;
use Helpers\Session;
use Helpers\Url;

class AdminController extends Controller {

    protected $language = null;
    protected $rubric = null;
    protected $faq = null;
    //protected $layout = "admin";

    public function __construct(ILanguageRepository $language, IRubricRepository $rubric, IFaqRepository $faq)
    {
        parent::__construct();

        $this->language = $language;
        $this->rubric = $rubric;
        $this->faq = $faq;
    }

    public  function index() {

        if (!Session::get('loggedin'))
        {
            Session::set('redirect', 'admin');
            Url::redirect('/');
        }

        if(!Session::get('isAdmin'))
        {
            Url::redirect('/rubric');
        }
		
        $data['menu'] = 1;
        $data['faqs'] = $this->faq->all();

        return View::make('Admin/Main/Index')
            ->shares("title", __("admin_project_title"))
            ->shares("data", $data);
    }


    public function updateLanguages()
    {
        $response = ["success" => false];

        if (!Session::get('loggedin'))
        {
            $response["error"] = __("not_loggedin_error");
            echo json_encode($response);
            exit;
        }

        if(!Session::get('isAdmin'))
        {
            $response["error"] = __("not_enough_rights_error");
            echo json_encode($response);
            exit;
        }

        $this->language->updateFromTd();

        // Insert vraft (admin) language record
        $this->language->insert([
            "langID" => "vraft",
            "langName" => "vRAFT Admin",
            "angName" => "vRAFT Admin",
            "isGW" => false,
            "gwLang" => "vRAFT",
            "direction" => "ltr"
        ]);

        $response["success"] = true;

        echo json_encode($response);
    }

    public function createFaq()
    {
        $result = ["success" => false];

        if (!Session::get('loggedin'))
        {
            $response["error"] = __("not_loggedin_error");
            echo json_encode($response);
            exit;
        }

        if(!Session::get('isAdmin'))
        {
            $response["error"] = __("not_enough_rights_error");
            echo json_encode($response);
            exit;
        }

        $question = Input::get("question", "");
        $category = Input::get("category", "common");
        $answer = Input::get("answer", "");

        if(trim($question) != "" && trim($answer) != ""
            && preg_match("/^common|vraft$/", $category))
        {
            $data = [
                "title" => $question,
                "text" => $answer,
                "category" => $category
            ];

            $q = $this->faq->create($data);
            if($this->faq->save($q))
            {
                $li = '<li class="faq_content" id="'.$q->id.'">
                            <div class="tools_delete_faq">
                                <span>'.__("delete").'</span>
                                <img src="'.template_url("img/loader.gif").'">
                            </div>

                            <div class="faq_question">'.$q->title.'</div>
                            <div class="faq_answer">'.$q->text.'</div>
                            <div class="faq_cat">'.__($q->category).'</div>
                        </li>';

                $result["success"] = true;
                $result["li"] = $li;
            }
        }

        echo json_encode($result);
    }


    public function deleteFaq()
    {
        $result = ["success" => false];

        if (!Session::get('loggedin'))
        {
            $response["error"] = __("not_loggedin_error");
            echo json_encode($response);
            exit;
        }

        if(!Session::get('isAdmin'))
        {
            $response["error"] = __("not_enough_rights_error");
            echo json_encode($response);
            exit;
        }

        $questionID = Input::get("id", 0);

        if($questionID)
        {
            $question = $this->faq->get($questionID);
            if($this->faq->delete($question))
            {
                $result["success"] = true;
            }
        }

        echo json_encode($result);
    }

    public function uploadImage()
    {
        $result = ["success" => false];

        if (!Session::get('loggedin'))
        {
            $response["error"] = __("not_loggedin_error");
            echo json_encode($response);
            exit;
        }

        if(!Session::get('isAdmin'))
        {
            $response["error"] = __("not_enough_rights_error");
            echo json_encode($response);
            exit;
        }

        $image_file = Input::file("image");

        if($image_file->isValid())
        {
            $mime = $image_file->getMimeType();
            if(in_array($mime, ["image/jpeg", "image/png", "image/gif", "application/pdf"]))
            {
                $fileExtension = $image_file->getClientOriginalExtension();
                $fileName = uniqid().".".$fileExtension;
                $destinationPath = "../app/Templates/Default/Assets/faq/";
                $image_file->move($destinationPath, $fileName);

                if(File::exists(join("/", [$destinationPath, $fileName])))
                {
                    $result["success"] = true;
                    $result["ext"] = $fileExtension;
                    $result["url"] = template_url("faq/".$fileName);
                    echo json_encode($result);
                    exit;
                }
            }
            else
            {
                $result["error"] = __("wrong_image_format_error");
                echo json_encode($result);
                exit;
            }
        }
        else
        {
            $result["error"] = __("error_ocured");
            echo json_encode($result);
            exit;
        }
    }
}
