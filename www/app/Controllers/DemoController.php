<?php
namespace App\Controllers;

use App\Repositories\Demo\IDemoRepository;
use App\Repositories\Faq\IFaqRepository;
use App\Repositories\Pdf\IPdfRepository;
use View;
use App\Core\Controller;
use Response;
use Helpers\Url;
use Input;

/**
 * Sample controller showing a construct and 2 methods and their typical usage.
 */
class DemoController extends Controller
{
    protected $demo = null;
    protected $pdf = null;

    /**
     * Call the parent construct
     * @param IDemoRepository $demo
     * @param IPdfRepository $pdf
     */
    public function __construct(IDemoRepository $demo, IPdfRepository $pdf)
    {
        parent::__construct();

        $this->demo = $demo;
        $this->pdf = $pdf;
    }

    public function before()
    {
        return parent::before();
    }


    /**
     * Define Index page title and load template files
     * @param null $page
     * @return View
     */
    public function index($page = null)
    {
        $data['menu'] = 3;

        if($page == null) URL::redirect("demo/qualities");

        switch ($page)
        {
            case "qualities":
                return View::make('Demo/RubricQualities')
                    ->shares("title", __("rubric_title"))
                    ->shares("data", $data);
                break;

            case "define":
                return View::make('Demo/RubricDefs')
                    ->shares("title", __("rubric_title"))
                    ->shares("data", $data);
                break;

            case "measurements":
                return View::make('Demo/RubricMeasurements')
                    ->shares("title", __("rubric_title"))
                    ->shares("data", $data);
                break;

            default:
                return View::make('Demo/ReadRubric')
                    ->shares("title", __("rubric_title"))
                    ->shares("data", $data);
                break;
        }


    }

    public function download($type, $cjk = false)
    {
        $rubric = $this->demo->getRubric();
        $this->pdf->download($rubric, $type, $cjk);
    }
}
