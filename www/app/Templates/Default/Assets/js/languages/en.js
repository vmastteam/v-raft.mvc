/**
 * Created by mXaln on 18.08.2016.
 */

var Language = {
    "yes"                       : "Yes",
    "no"                        : "No",
    "saveChangesConfirm"        : "There are some changes made on this page. Do you want to continue without saving?",
    "saveChangesConfirmTitle"   : "Changes made",
    "seeAll"                    : "See all",
    "delete"                    : "Delete",
    "create"                    : "Create",
    "edit"                      : "Edit",
    "save"                      : "Save",
    "searchingFor"              : "searching for",
    "noResultText"              : "No results match",
    "statusOnline"              : "online",
    "statusOffline"             : "offline",
    "searchEmpty"               : "Search gave no results",
    "searchMore"                : "Load more...",
    "connectionLostMessage"     : "Internet connection lost. You can't leave this page until connection is up.",
    "commonError"               : "An error occurred. It could be due to the server error or lost internet connection. Please, try again later.",
    "add"                       : "Add",
    "atLeastOneItemError"       : "There should be at least one item",
    "attention"                 : "Attention",
    "delQuestion"               : "Do you want to delete the question?"
};