$(document).ready(function() {

    Offline.options = {
        checks: {
            xhr: {
                url: "/rpc/check_internet"
            }
        }
    }

    Offline.on("confirmed-up", function () {
        console.log("internet is up");
    });

    Offline.on("confirmed-down", function () {
        console.log("internet is down");
    });

    $(window).bind('beforeunload', function(e){
        //return 'Are you sure you want to leave?';
    });

    $("a, button").click(function (e) {
        if(Offline.state == "up")
            return true;

        renderPopup(Language.connectionLostMessage);
        return false;
    });

    setTimeout(function () {
        $('[data-toggle="tooltip"]').tooltip();
    }, 1000);

    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
        _renderItem: function( ul, item ) {
            var li = $( "<li>" ),
                wrapper = $( "<div>", { text: item.label } );

            if ( item.disabled ) {
                li.addClass( "ui-state-disabled" );
            }

            $( "<span>", {
                style: item.element.attr( "data-style" ),
                "class": "ui-icon " + item.element.attr( "data-class" )
            })
                .appendTo( wrapper );

            return li.append( wrapper ).appendTo( ul );
        }
    });

    // Tabs switch
    $(".my_tab").click(function () {
        var id = $(this).attr("id");

        $(".my_content").removeClass("shown");
        $(".my_tab").removeClass("active");

        $(this).addClass("active");
        $("#"+id+"_content").addClass("shown");

        return false;
    });

    if(!$(".my_tab:first").hasClass("active"))
        $(".my_tab:first").addClass("active");

    if(!$(".my_content:first").hasClass("shown"))
        $(".my_content:first").addClass("shown");

    $( "#lang-select" )
        .iconselectmenu({
            width: 60,
            create: function( event, ui ) {
                var lang = getCookie("lang");
                lang = typeof lang != "undefined" ? lang : "en";
                $("#lang-select-button .ui-selectmenu-text").html(
                    "<img src='/app/templates/default/img/" + lang + ".png' width='16' height='12'>"
                );
            },
            select: function( event, ui ) {
                $("#lang-select-button .ui-selectmenu-text").html(
                    "<img src='/app/templates/default/img/" + ui.item.value + ".png' width='16' height='12'>"
                );
                window.location.href = "/lang/" + ui.item.value;
            }
        })
        .iconselectmenu( "menuWidget" )
        .addClass( "ui-menu-icons customicons" );

    // Statement of faith block
    $("#sof").click(function() {
        $('#sof_modal').modal('show');
        return false;
    });

    $("#sof_agree").click(function() {
        $("#sof").prop('checked', true);
        $('#sof_modal').modal('hide');
        $("#sof").parents("label").popover('destroy');
    });

    $("#sof_cancel").click(function() {
        $("#sof").prop('checked', false);
        $('#sof_modal').modal('hide');
    });

    // Terms of use block
    $("#tou").click(function() {
        $('#tou_modal').modal('show');
        return false;
    });

    $("#tou_agree").click(function() {
        $("#tou").prop('checked', true);
        $("#tou").parents("label").popover('destroy');
        $('#tou_modal').modal('hide');
    });

    $("#tou_cancel").click(function() {
        $("#tou").prop('checked', false);
        $('#tou_modal').modal('hide');
    });

    // Language Select
    $(".rubric_starter select").change(function() {
        var lang = $(this).val();

        $(".passwordreset").attr("href", "passwordreset/" + lang);

        $.ajax({
            url: "/rubric/test/" + lang,
            method: "post",
            dataType: "json",
            beforeSend: function() {
                $(".rubricLoader").show();
            }
        })
        .done(function(data) {
            if(typeof data.rubric != "undefined" && data.rubric)
            {
                $(".create_rubric").hide();
                $(".login_rubric").show();
                $(".rubric_starter input[name=type]").val('login');
            }
            else
            {
                $(".create_rubric").show();
                $(".login_rubric").hide();
                $(".rubric_starter input[name=type]").val('create');
            }
        })
        .always(function() {
            $(".rubricLoader").hide();
        });
    });

    // Add new rubric item
    $(".rubric_item_add button").click(function() {
        var dir = $(this).data("dir");
        var item = '' +
            '<div class="rubric_item_block">' +
                '<span></span> ' +
                '<textarea class="rubric_item_orig" rows="2" dir="'+dir+'"></textarea> ' +
                '<button class="btn btn-danger rubric_item_delete glyphicon glyphicon-remove" title="'+Language.delete+'"></button> ' +
                '<textarea class="rubric_item_eng" rows="2"></textarea> ' +
            '</div>' +
        '';
        
        var parentBlock = $(this).parents(".parent_block");
        parentBlock.find(".rubric_items").append(item);
        setItemNumbers(parentBlock);
    });

    // Delete rubric item (quality, definition, question)
    $(document).on("click", ".rubric_item_delete", function() {
        var $this = $(this);
        var parent = $this.parents(".rubric_item_block");
        var parentBlock = $(this).parents(".parent_block");
        var id = parent.data("id");
        if(parentBlock.find(".rubric_item_block").length > 1)
        {
            if(typeof id != "undefined")
            {
                if(typeof isDemo != "undefined" && isDemo)
                {
                    parent.remove();
                    setItemNumbers(parentBlock);
                    $(".rubric_navigate")[0].dispatchEvent(navigateEvent);
                    return false;
                }

                var type = $(".rubric_items").data("type");
                $.ajax({
                    url: "/rubric/"+type+"/" + id,
                    method: "delete",
                    dataType: "json",
                    beforeSend: function() {
                        $this.prop("disabled", true);
                        parent.append('<img src="/templates/default/assets/img/loader.gif" />');
                    }
                })
                .done(function(data) {
                    if(typeof data.success != "undefined")
                    {
                        parent.remove();
                        setItemNumbers(parentBlock);
                        $(".rubric_navigate")[0].dispatchEvent(navigateEvent);
                    }
                    else
                    {
                        renderPopup(data.error);
                    }
                })
                .always(function() {
                    $this.prop("disabled", false);
                    parent.find('img').remove();
                });
            }
            else
            {
                $this.parents(".rubric_item_block").remove();
                setItemNumbers(parentBlock);
            }
        }
        else
        {
            renderPopup(Language.atLeastOneItemError);
        }
    });

    // Cache item old text
    $(document).on("focus", ".rubric_item_orig, .rubric_item_eng", function() {
        var parent = $(this).parents(".rubric_item_block");
        parent.data("orig", parent.find(".rubric_item_orig").val());
        parent.data("eng", parent.find(".rubric_item_eng").val());
    });

    // Save changed item content to database
    $(document).on("blur", ".rubric_item_orig, .rubric_item_eng", function() {
        if(typeof isDemo != "undefined" && isDemo) return false;

        $this = $(this);
        var parent = $(this).parents(".rubric_item_block");
        var orig = parent.find(".rubric_item_orig").val();
        var eng = parent.find(".rubric_item_eng").val();
        var parentBlock = $this.parents(".parent_block");
        var parentID = parentBlock.find(".rubric_items").data("parentid");
        var type = $(".rubric_items").data("type");
        
        if(orig != parent.data("orig") || eng != parent.data("eng"))
        {
            var id = parent.data("id");
            var method = "put";

            // Check should we create new item or update one that exists
            // If id is defined than update item otherwise create new one
            if(id === undefined)
            {
                id = parentID;
                method = "post";
            }
                    
            $.ajax({
                url: "/rubric/"+type+"/" + id,
                method: method,
                dataType: "json",
                data: {
                    "orig": orig,
                    "eng": eng
                },
                beforeSend: function() {
                    parent.find(".rubric_item_delete").prop("disabled", true);
                    parent.append('<img src="/templates/default/assets/img/loader.gif" />');
                }
            })
            .done(function(data) {
                if(typeof data.success != "undefined")
                {
                    if(typeof data.id != "undefined")
                    {
                        parent.attr("data-id", data.id);
                    }
                    $(".rubric_navigate")[0].dispatchEvent(navigateEvent);
                }
                else
                {
                    renderPopup(data.error);
                }
            })
            .error(function(jqXHR, textStatus, errorThrown) {
                renderPopup(textStatus + ": " + errorThrown);
            })
            .always(function() {
                parent.find(".rubric_item_delete").prop("disabled", false);
                parent.find('img').remove();
            });
        }
    });

    // Navigate to another page when saving/updating/deleting is done
    $(".rubric_navigate").click(function() {
        var href = $(this).attr("href");
        if(!isWorking)
        {
            window.location = href;
        }
        else
        {
            this.addEventListener('navigate', function (e) {
                window.location = href;
            }, false);
        }
    });

    // Show/hide original/english content of a rubric
    $(".read_rubric_tabs li").click(function(e) {
        e.preventDefault();
        var id = $(this).attr("id");
        $(this).addClass("active");

        if(id == "tab_orig")
        {
            $("#tab_eng").removeClass("active");
            $(".read_rubric_qualities .orig").show();
            $(".read_rubric_qualities .eng").hide();
        }
        else
        {
            $("#tab_orig").removeClass("active");
            $(".read_rubric_qualities .orig").hide();
            $(".read_rubric_qualities .eng").show();
        }
    });

    $(".panel-close").click(function() {
        $(this).parents(".form-panel").css("left", -9999);
    });

    function localizeDate() {
        $.each($(".datetime"), function () {
            var dateStr = $(this).attr("data");
            if(dateStr == "" || dateStr == "----/--/--" || dateStr == "--:--") return true;

            var date_options = {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                weekday: 'short',
                timezone: 'UTC',
            };

            var time_options = {
                hour: 'numeric',
                minute: 'numeric',
                timezone: 'UTC',
            };

            if($(".event_time_time").length <= 0) // Combine date and time options for full result
            {
                date_options.hour = 'numeric';
                date_options.minute = 'numeric';
            }

            var lang = getCookie("lang") != "undefined" ? getCookie("lang") : "en";

            var date = new Date(dateStr + " UTC");
            $(this).text(date.toLocaleString(lang, date_options));
            $(this).next(".event_time_time").text(date.toLocaleString(lang, time_options));
        });
    }

    localizeDate();

    $("#cjk_enable").click(function () {
        var parent = $(this).parents(".orig");
        var cjk = $(this).is(":checked");
        var href = $("a", parent).prop("href");

        if(cjk)
        {
            href += "cs";
            $(".cjk_options").show();
        }
        else
        {
            href = href.replace(/[a-z]+$/, "");
            $(".cjk_options").hide();
        }

        $("a", parent).prop("href", href);
    });

    $("body").on("click", ".cjk", function () {
        var parent = $(this).parents(".orig");
        var href = $("a", parent).prop("href");
        var id = $(this).attr("id");

        href = href.replace(/[a-z]+$/, id);
        $("a", parent).prop("href", href);
    });

    $("#lang_filter").keyup(function () {
        var val = $(this).val();
        var re = new RegExp(val, 'i');
        $(".lang_item").each(function () {
            if(!$(this).text().match(re))
                $(this).hide();
            else
                $(this).show();
        });
    });
});


// Variables
var isWorking = false;
var navigateHref = "";
var navigateEvent = new Event('navigate');

// Cookie Helpers
/**
 * Get cookie by name
 * @param string name
 * @returns {*}
 */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

/**
 * Set cookie value by name
 * @param string name
 * @param string value
 * @param object options
 */
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function setItemNumbers(parent)
{
    var items = [];
    if(parent)
    {
        items = parent.find(".rubric_item_block");
    }
    else
    {
        items = $(".rubric_item_block");
    }
    
    $.each(items, function(i, v) {
        $(v).find('span').text(zFill(i+1, 2) + '.');
    });
}

function zFill(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

/**
 * Delete cookie by name (make it expired)
 * @param string name
 */
function deleteCookie(name, options) {
    setCookie(name, "", {
        expires: -1,
        path: "/"
    })
}

function unEscapeStr(string) {
    return $('<div/>').html(string).text();
}

/**
 * Renders and shows dialog window with OK button
 * @param message
 * @param onOK Ok button callback
 * @returns {boolean}
 */
function renderPopup(message, onOK, onClose) {
    onOK = typeof onOK != "undefined" ? onOK : function(){
        $( this ).dialog( "close" );
    };

    onClose = typeof onClose != "undefined" ? onClose : function(){
            $( this ).dialog( "close" );
            return false;
        };

    $(".alert_message").html(message);
    $( "#dialog-message" ).dialog({
        modal: true,
        resizable: false,
        draggable: false,
        width: 500,
        buttons: {
            Ok: onOK
        },
        close: onClose,
    });

    return true;
}

/**
 * Renders and shows confirm dialog popup
 * @param title
 * @param message
 * @param onAnswerYes positive answer callback
 * @param onAnswerNo Negative answer callback
 * @param onClose close dialog callback
 */
function renderConfirmPopup(title, message, onAnswerYes, onAnswerNo, onClose) {
    onAnswerYes = typeof onAnswerYes != "undefined" ? onAnswerYes : function(){
        $( this ).dialog( "close" );
        return true;
    };
    onAnswerNo = typeof onAnswerNo != "undefined" ? onAnswerNo : function(){
        $( this ).dialog( "close" );
        return false;
    };
    onClose = typeof onClose != "undefined" ? onClose : function(){
            $( this ).dialog( "close" );
            return false;
        };

    var yes = Language.yes;
    var no = Language.no;

    var btns = {};
    btns[yes] = onAnswerYes;
    btns[no] = onAnswerNo;

    $(".confirm_message").html(message);
    $( "#check-book-confirm" ).dialog({
        resizable: false,
        draggable: false,
        title: title,
        height: "auto",
        width: 500,
        modal: true,
        buttons: btns,
        close: onClose,
    });
}


/**
 * Detect if current browser is Internet Explorer/Edge
 * @returns {boolean}
 */
function isIE() {
    if (/MSIE 10/i.test(navigator.userAgent)) {
        // this is internet explorer 10
        return true;
    }

    if(/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)){
        // this is internet explorer 9 and 11
        return true;
    }

    if (/Edge\/12./i.test(navigator.userAgent)){
        // this is Microsoft Edge
        return true;
    }

    return false;
}

function debug(obj, stop) {
    stop = stop || false;
    console.log(obj);
    if(stop)
        throw new Error("debug stop!");
}

jQuery.fn.deserialize = function (data) {
    var f = this,
        map = {},
        find = function (selector) { return f.is("form") ? f.find(selector) : f.filter(selector); };
    //Get map of values
    jQuery.each(data.split("&"), function () {
        var nv = this.split("="),
            n = decodeURIComponent(nv[0]),
            v = nv.length > 1 ? decodeURIComponent(nv[1]) : null;
        if (!(n in map)) {
            map[n] = [];
        }
        map[n].push(v);
    })
    //Set values for all form elements in the data
    jQuery.each(map, function (n, v) {
        find("[name='" + n + "']").val(v);
    })
    //Clear all form elements not in form data
    find("input:text,select,textarea").each(function () {
        if (!(jQuery(this).attr("name") in map)) {
            jQuery(this).val("");
        }
    })
    find("input:checkbox:checked,input:radio:checked").each(function () {
        if (!(jQuery(this).attr("name") in map)) {
            this.checked = false;
        }
    })
    return this;
};
