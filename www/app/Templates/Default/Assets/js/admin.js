/**
 * Created by Maxim on 05 Feb 2016.
 */

// ------------------ Jquery Events --------------------- //

$(function () {
    $(".panel-close").click(function() {
        $(this).parents(".form-panel").css("left", "-9999px");
    });

    // Admin tools

    // Update languages database
    $(".update_langs button").click(function () {
        $.ajax({
            url: "/admin/languages/update",
            method: "post",
            dataType: "json",
            beforeSend: function() {
                $(".update_langs img").show();
            }
        })
            .done(function(data) {
                if(data.success)
                {
                    renderPopup("Updated!");
                }
                else
                {
                    if(typeof data.error != "undefined")
                    {
                        renderPopup(data.error);
                    }
                }
            })
            .always(function() {
                $(".update_langs img").hide();
            });
    });

    // Delete question
    $("body").on("click", ".tools_delete_faq", function (e) {
        var li = $(this).parent("li");
        var questionID = li.attr("id");

        renderConfirmPopup(Language.attention, Language.delQuestion, function () {
            $( this ).dialog( "close" );
            $.ajax({
                url: "/admin/faq/delete",
                method: "post",
                data: {id: questionID},
                dataType: "json",
                beforeSend: function() {
                    $("img", li).show();
                }
            })
                .done(function(data) {
                    if(data.success)
                    {
                        li.remove();
                    }
                    else
                    {
                        if(typeof data.error != "undefined")
                        {
                            renderPopup(data.error);
                        }
                    }
                })
                .always(function() {
                    $("img", li).hide();
                });
        });

        e.preventDefault();
        return false;
    });

    // Create Question
    $("body").on("click", ".faq_create .create_faq", function (e) {
        var question = $("#faq_question").val();
        var answer = $("#faq_answer").val();
        var category = $("#faq_category").val();

        $.ajax({
            url: "/admin/faq/create",
            method: "post",
            data: {
                question: question,
                answer: answer,
                category: category
            },
            dataType: "json",
            beforeSend: function() {
                $("#faq_create_loader").show();
            }
        })
            .done(function(data) {
                if(data.success)
                {
                    $("#faq_question").val("");
                    $("#faq_answer").val("");
                    $("#faq_category").val("");
                    $(".faq_list.tools ul").prepend(data.li);
                    $('#faq_answer').summernote('reset');
                }
                else
                {
                    if(typeof data.error != "undefined")
                    {
                        renderPopup(data.error);
                    }
                }
            })
            .always(function() {
                $("#faq_create_loader").hide();
            });

        e.preventDefault();
        return false;
    });

    // Upload image for FAQ
    $('#faq_answer').summernote({
        dialogsInBody: true,
        height: 300,
        minHeight: null,
        maxHeight: null,
        shortCuts: false,
        disableDragAndDrop: false,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['Insert', ['video', 'picture', 'link', 'table', 'hr']],
            ['Other', ['codeview', 'undo', 'redo', 'fullscreen']]
        ],
        callbacks: {
            onImageUpload: function(image) {
                uploadImageContent(image[0], $(this));
            }
        }
    });
});


// --------------- Variables ---------------- //



// --------------- Functions ---------------- //
function uploadImageContent(image, editor) {
    var data = new FormData();
    data.append("image", image);
    $.ajax({
        url: "/admin/image/upload",
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        dataType: "json",
        type: "post",
        success: function(data) {
            console.log(data);
            if(data.success) {
                if(data.ext == "pdf") {
                    $(editor).summernote('createLink', {
                        text: "Set link name",
                        url: data.url,
                        isNewWindow: true
                    });
                } else {
                    var image = $("<img>").attr("src", data.url);
                    $(editor).summernote("insertNode", image[0]);
                }
            } else {
                renderPopup(data.error);
            }
        },
        error: function(data) {
            console.log(data);
        }
    });
}