<?php
/**
 * Default Layout - a Layout similar with the classic Header and Footer files.
 */

use Helpers\Session;
use Helpers\Url;

$language = Language::code();
$languages = Config::get('languages');
setcookie("lang", $language, time() + 365*24*3600, "/");

$languageFull = "";

switch ($language) {
    case 'ru':
        $languageFull = "ru-RU";
        break;

    case 'id':
        $languageFull = "id-ID";
        break;

    default:
        $languageFull = "en-US";
        break;
}

if(!isset($data)) $data = ["menu" => 1];
?>
<!DOCTYPE html>
<html lang="<?=$language; ?>">
<head>
    <meta charset="utf-8">
    <title><?= $title .' - ' .Config::get('app.name', SITETITLE); ?></title>

    <link rel="icon" href="<?php echo template_url("favicon.ico") ?>" type="image/x-icon" />
<?php
echo isset($meta) ? $meta : ''; // Place to pass data / plugable hook zone

Assets::css([
    template_url('css/bootstrap.min.css'),
    template_url('css/style.css?8'),
    template_url('css/jquery-ui.min.css'),
    template_url('css/jquery-ui.structure.min.css'),
    template_url('css/jquery-ui.theme.min.css'),
    template_url('css/summernote.css'),
]);

echo isset($css) ? $css : ''; // Place to pass data / plugable hook zone

Assets::js([
    template_url('js/jquery.js'),
    template_url('js/languages/'.$language.'.js?7'),
    template_url('js/main.js?9', 'Default'),
    (Session::get("isAdmin") ?  template_url('js/admin.js?6') : ''),
    template_url('js/bootstrap.min.js'),
    template_url('js/autosize.min.js'),
    template_url('js/jquery-ui.min.js'),
    template_url('js/offline.min.js'),
    template_url('js/summernote/summernote.min.js'),
    ($languageFull != "en-US" ? template_url('js/i18n/summernote-'.$languageFull.'.js') : ""),
]);

echo isset($js) ? $js : ''; // Place to pass data / plugable hook zone
?>
</head>
<body class="header_bg">

<?= isset($afterBody) ? $afterBody : ''; // Place to pass data / plugable hook zone ?>

<div class="container">

    <div class="header page-header row <?php echo Session::get("loggedin") ? "loggedin" : ""?>">

        <div class="col-sm-8 row header_menu_left">
            <a href="/" class="col-sm-6 logo"><img src="<?php echo Url::templatePath() ?>img/logo.png?1" height="40" /></a>

            <ul class="nav nav-pills col-sm-6" role="tablist">
                <?php if(Session::get('loggedin')): ?>
                    <li <?php if($data['menu'] == 1):?>class="active"<?php endif?> role="presentation"><a href="/rubric"><?php echo __('home')?></a></li>
                    <li <?php if($data['menu'] == 2):?>class="active"<?php endif?> role="presentation"><a href="/rubric/all"><?php echo __('rubrics_title')?></a></li>
                    <li <?php if($data['menu'] == 3):?>class="active"<?php endif?> role="presentation"><a href="/demo"><?php echo __('demo')?></a></li>
                    <li <?php if($data['menu'] == 4):?>class="active"<?php endif?> role="presentation"><a href="/faq"><?php echo __('faq')?></a></li>
                <?php else: ?>
                    <li <?php if($data['menu'] == 1):?>class="active"<?php endif?> role="presentation"><a href="/"><?php echo __('home')?></a></li>
                    <?php if($data['menu'] > 1):?>
                    <li <?php if($data['menu'] == 2):?>class="active"<?php endif?> role="presentation"><a href="/rubric/all"><?php echo __('rubrics_title')?></a></li>
                    <li <?php if($data['menu'] == 3):?>class="active"<?php endif?> role="presentation"><a href="/demo"><?php echo __('demo')?></a></li>
                    <?php endif; ?>
                    <li <?php if($data['menu'] == 4):?>class="active"<?php endif?> role="presentation"><a href="/faq"><?php echo __('faq')?></a></li>
                <?php endif; ?>
            </ul>
        </div>

        <ul class="list-inline col-sm-4 header_menu_right">
            <?php if(Session::get('loggedin')): ?>
                <li>
                    <div class="profile-select">
                        <div class="dropdown-toggle" id="profile-select" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <div class="uName"><?php echo Session::get("langname")?></div>
                            <span class="caret"></span>
                        </div>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="profile-select">
                            <li><a href="/logout"><?php echo __('logout')?></a></li>
                        </ul>
                    </div>
                </li>
            <?php else: ?>
                <li>
                    <div class="dropdown flangs">
                        <div class="dropdown-toggle" id="footer_langs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="<?php echo template_url("img/" . $language . ".png", "Default"); ?>">
                            <span class="caret"></span>
                        </div>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="footer_langs">
                            <?php foreach ($languages as $code => $lang): ?>
                            <li>
                                <a href="/language/<?php echo $code ?>" title="<?php echo $lang['info']; ?>">
                                    <img src="<?php echo template_url("img/".$code.".png", "Default") ?>"> <?php echo $lang['name']; ?>
                                </a>
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </li>
            <?php endif?>
        </ul>

    </div>

    <div class="container_block isloggedin">
        <!-- dialog windows -->
        <div id="check-book-confirm" title="" style="display: none">
            <br>
            <p>
                <span class="glyphicon glyphicon glyphicon-alert" style="float:left; margin: 7px 20px 40px 0; font-size: 30px; color: #f00;"></span>
                <span class="confirm_message"><?php echo __("check_book_confirm") ?></span>
            </p>
        </div>

        <div id="dialog-message" title="<?php echo __("alert_message") ?>" style="display: none">
            <br>
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:3px 7px 30px 0;"></span>
                <span class="alert_message"></span>
            </p>
        </div>

        <?= $content; ?>
    </div>

    <footer class="footer">
        <div class="container-fluid">
            <div class="row" style="margin: 15px 0 0;">
                <div class="col-lg-3">
                    <p class="text-muted">Copyright &copy; <?php echo date('Y'); ?> <b>V-Raft.com</b></p>
                </div>
                <div class="col-lg-7">
                    <p class="text-muted pull-right">
                        <?php if(Config::get('app.debug')) { ?>
                        <small><!-- DO NOT DELETE! - Profiler --></small>
                        <?php } ?>
                    </p>
                </div>
                <div class="col-sm-2 footer_langs">
                    <?php if(Session::get("loggedin")): ?>
                        <div class="dropup flangs">
                            <div class="dropdown-toggle" id="footer_langs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo template_url("img/".$language.".png") ?>">
                                <span class="caret"></span>
                            </div>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="footer_langs">
                                <?php foreach ($languages as $code => $lang): ?>
                                    <li>
                                        <a href="/language/<?php echo $code ?>" title="<?php echo $lang['info']; ?>">
                                            <img src="<?php echo template_url("img/".$code.".png") ?>"> <?php echo $lang['name']; ?>
                                        </a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </footer>

<?php
echo isset($footer) ? $footer : ''; // Place to pass data / plugable hook zone
?>
</div>

<!-- DO NOT DELETE! - Forensics Profiler -->

</body>
</html>
