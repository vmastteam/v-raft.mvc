<?php
/**
 * All known Languages
 *
 * @author David Carr - dave@daveismyname.com
 * @author Virgil-Adrian Teaca - virgil@giulianaeassociati.com
 * @version 3.0
 */

use Config\Config;


Config::set('languages', array(
    'en' => array('info' => 'English',   'name' => 'English',    'locale' => 'en-US', 'dir' => 'ltr'),
    'ru' => array('info' => 'Russian',   'name' => 'Русский',    'locale' => 'ru-RU', 'dir' => 'ltr'),
    //'ceb' => array('info' => 'Cebuano',  'name' => 'Cebuano',    'locale' => 'ceb',   'dir' => 'ltr'),
    //'id' => array('info' => 'Indonesian',  'name' => 'Bahasa Indonesia',    'locale' => 'id',   'dir' => 'ltr'),
));