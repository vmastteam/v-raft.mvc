<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 29 Feb 2016
 * Time: 19:41
 */

namespace Helpers\Constants;


class EventStates
{
    const STARTED       = "started";
    const TRANSLATING   = "translating";
    const TRANSLATED    = "translated";
    const L2_RECRUIT    = "l2_recruit";
    const L2_CHECK      = "l2_check";
    const L3_RECRUIT    = "l3_recruit";
    const L3_CHECK      = "l3_check";
    const COMPLETE      = "complete";
}