<?php
/**
 * Created by PhpStorm.
 * User: Maxim
 * Date: 29 Feb 2016
 * Time: 20:33
 */

namespace Helpers\Constants;


class BookSources
{
    const catalog = array(
        "en" => array("ulb", "udb"),
        "ru" => array("rsb"),
        "ar" => array("avd"),
        //"hwc" => array("hce"),
        //"hu" => array("kar"),
        //"sr-Latn" => array("dkl"),
        "ceb" => array("ulb", "udb"),
        "id" => array("ulb")
    );
}